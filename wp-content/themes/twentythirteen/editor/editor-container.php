<select id="databases" style="margin-bottom:1em;">
	<?php
		$result = mysql_query("SHOW DATABASES LIKE 'deadlyp\_%'");
		while ($row = mysql_fetch_array($result)) {
		    echo "<option>" . $row[0]. "</option>";
		}
	?>
</select>

<?php
	$database = "deadlyp_mary_rotter";
	$table = "game";
?>
		<select size="25" id="<?php echo $table ?>-fields">
			<?php
				$result = mysql_query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$database' AND TABLE_NAME = '$table'");        
				while ($row = mysql_fetch_array($result)) {
				    echo "<option>" . $row[0] . "</option>";
				}
			?>
		</select>
<?php
	$database = $_POST['database'];
?>
<ul class="tabs-menu">
	<li data-tab="game" class="active">Game</li>
	<li data-tab="players">Players</li>
</ul>
<div class="tabs-content">
	<div class="block active" data-tab="game" style="overflow:hidden;">
		<?php
			$table = "game";
			include "editor.php";
		?>
	</div>
	<div class="block" data-tab="players" style="overflow:hidden;">
		<?php
			$table = "players";
			include "editor.php";
		?>
	</div>
</div><!-- tabs-content -->
<script>
	
	// load content

	jQuery(".tabs-content option").click(function() {
		var field = jQuery(this).html();
		var table = jQuery(this).parents(".block").attr("data-tab");
		jQuery("#load").load("/wp-content/themes/twentythirteen/editor/load-content.php", {
			database: database,
			table: table,
			field: field
		});
		var value = jQuery("#load").html();
		jQuery("#load").html("");
		jQuery("#" + table + "-content").val(value);
	});
	
	// update content
	
	jQuery("textarea").keyup(function() {
		var table = jQuery(this).parents(".block").attr("data-tab");
		var field = jQuery("#" + table + "-fields").val();
		var value = jQuery(this).val();
		jQuery("#load").load("/wp-content/themes/twentythirteen/editor/update-content.php", {
			database: database,
			table: table,
			field: field,
			value: value
		});
	});
	
</script>