<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="bg"></div>
	<div class="content">
	<header class="entry-header">

		<?php if ( is_single() ) : ?>
		<h1 class="entry-title">
			<?php
				$full_title = get_field('full_title');
				if ($full_title == null) the_title();
				else the_field('full_title');
				$category = get_the_category(); 
				$category = $category[0]->cat_name;
				if (($category == "Store") || ($category == "Downloads")) {
			?>
			<small>Murder Mystery Party Game</small>
			<?php } ?>
		</h1>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h1>
		<?php endif; // is_single() ?>
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<?php endif; ?>

		<div class="entry-meta">
			<!--<?php twentythirteen_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>-->
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php
			$category = get_the_category(); 
			$category = $category[0]->cat_name;
			if ($category == "Party Ideas") {
				if ( has_post_thumbnail() && ! post_password_required() ) { ?>			
					<a href="<?php the_permalink(); ?>" rel="bookmark">
						<div class="entry-thumbnail alignleft">
							<?php the_post_thumbnail();
							echo "<div class='caption'>" . get_post(get_post_thumbnail_id())->post_excerpt . "</div>"; ?>
						</div>
					</a>
		<?php
				}
			}
		?>

		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php echo $download ?>
		<?php
			$category = get_the_category(); 
			$category = $category[0]->cat_name;
			if (($category == "Store") || ($category == "Downloads")) {
				$game_db = get_field('game_db');
				$download_id = get_field('download_id');
				$con=mysqli_connect("localhost","root","Penguin4Shpenguin!?",$game_db);
				$result = mysqli_query($con,"SELECT * FROM game");
				while($row = mysqli_fetch_array($result)) {
					$title = $row['title'];
					$author = $row['author'];
					$description = $row['description'];
					$rating = $row['rating'];
					$pages = $row['pages'];
					$backStory = $row['back_story'];
					$setting = $row['setting'];
					$url = str_replace(" ", "-", $title);
					$url = strtolower($url);
				}
			}
			if ($category == "Downloads" && !(is_single(138))) { ?>
				<img src="<?php echo get_template_directory_uri() ?>/images/pdf-50.png" alt="PDF" style="max-width:50px;">
				<a class="button" href="http://deadlyparties.com/?s2member_file_download=<?php echo $title ?>.pdf&s2member_file_download_key=<?php echo s2member_file_download_key($title . ".pdf"); ?>">Download PDF</a>
				<img src="<?php echo get_template_directory_uri() ?>/images/web-app-50.png" alt="web app" style="max-width:50px;">
				<a class="button" href="http://deadlyparties.com/<?php echo $url ?>">Web App</a>
		<?php
			} if ($category == "Downloads" && !(is_single(138))) { ?>
				
				
			<?php } if ($category == "Store") {
		?>
		<div class="column" style="position:relative;">
			<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover.jpg" alt="<?php echo $title ?>">
			<?php
				$discount = get_field('discount');
				if ($discount !== "0") {
			?>
				<div class="discount"><?php the_field('discount') ?>%<br>Off</div>
			<?php } ?>
		</div><!-- column -->
		<div class="column">
			<p style="margin-top:0;"><?php echo $description ?></p>
			<table class="gray mini-table">
				<tr>
					<th>Players:</th>
					<td>
						<?php
							$result = mysqli_query($con,"SELECT count(1) FROM players");
							$row = mysqli_fetch_array($result);
							$total = $row[0];
							echo $total;
						?>
					</td>
				</tr>
				<tr>
					<th>Gameplay:</th>
					<td>3 - 4 Hours</td>						
				</tr>
				<tr>
					<th>Ages:</th>
					<td>13 &uarr;</td>						
				</tr>
				<tr>
					<th>Rating:</th>
					<td><?php echo $rating ?></td>						
				</tr>
				<tr>
					<th>Pages:</th>
					<td>&asymp; 65</td>						
				</tr>
			</table>
			<table class="gray mini-table">
				<tr>
					<th>Formats:</th>
					<td>
						<img src="<?php echo get_template_directory_uri() ?>/images/pdf-50.png" alt="PDF" style="max-width:50px;">
						<div class="info" onclick="pop('pdf')">?</div>
						<div class="pop" id="pdf">
							<h3 style="margin-top:0;">Adobe PDF Format</h3>
							<img src="<?php echo get_template_directory_uri() ?>/images/pdf.png" alt="PDF" style="max-width:140px;" class="alignleft">
							<p>If you prefer holding physical instructions, use the PDF format to make prints. The document also includes invitation templates, name tags, clues, and other resources that can be printed.</p>
						</div>
						<img src="<?php echo get_template_directory_uri() ?>/images/web-app-50.png" alt="web app" style="max-width:50px;">
						<div class="info" onclick="pop('web-app')">?</div>
						<div class="pop" id="web-app">
							<h3 style="margin-top:0;">Web App</h3>
							<img src="<?php echo get_template_directory_uri() ?>/images/web-app.png" alt="web app" style="max-width:140px;" class="alignleft">
							<p>Use the web app for extra convenience and to cut down on printing costs. Each of your guests can access the game through any mobile device, laptop, or desktop.</p>
						</div>
					</td>
				</tr>
				<?php if (get_field('discount') != "0") { ?>
					<tr>
						<th>Retail:</th>
						<td>
							<div class="strike">$40.00<div class="line"></div></div>
						</td>
					</tr>
					<tr>
						<th>Wholesale:</th>
						<td>
							$9.99<br>
							<small>($1.25/player)</small>
						</td>
					</tr>
				<?php } else { ?>
					<tr>
						<th>Price:</th>
						<td>
							<div>$40.00</div>
						</td>
					</tr>				
				<?php } ?>
			</table>
			<div class="clear"></div>
			<?php if (get_field('unavailable') == true) { ?>
				<button class="disabled small" style="margin-top:1em;">This Product is Temporarily Unavailable</button>
			<?php } else { ?>
				<button onclick="pop('purchase-type')">Buy Now</button>
			<?php } ?>
			<a href="/bulk-savings" class="button">Bulk Savings</a>
		</div><!-- column -->
		<div class="clear"></div>
		<br>
		

<ul class="tabs-menu">
	<li class="active gray" data-tab="details">Details</li>
	<li class="gray" data-tab="what-you-get">What You Get</li>
	<li class="gray" data-tab="backstory">Backstory</li>
	<li class="gray" data-tab="characters">Characters</li>
</ul>
<div class="clear"></div>
<div class="tabs-content">
	<div class="active block" data-tab="details">
		<h2>Details</h2>
		<ul>
			<li>Formats: Digital PDF / Web App</li>
			<li>Players:
				<?php
					$result = mysqli_query($con,"SELECT count(1) FROM players");
					$row = mysqli_fetch_array($result);
					$total = $row[0];
					echo $total;
				?>
				<?php
					$result = mysqli_query($con,"SELECT count(1) FROM players WHERE sex = 'male'");
					$row = mysqli_fetch_array($result);
					$male = $row[0];
					echo "(" . $male;
				?>
				males, 
				<?php
					$result = mysqli_query($con,"SELECT count(1) FROM players WHERE sex = 'female'");
					$row = mysqli_fetch_array($result);
					$female = $row[0];
					echo $female . " females)";
				?>	
			</li>
			<li>Rating: <?php echo $rating ?></li>
			<li>Recommended Ages: 13 and up</li>
			<li>Approximate Pages: 65 single-sided / 33 double-sided</li>
			<li>Approximate Gameplay: 3 - 4 Hours</li>
			<li>Setting: <?php echo $setting ?></li>
			<li>Author(s): <?php echo $author ?></li>
		</ul>
	</div><!-- details -->
	<div class="block" data-tab="what-you-get">
		<h2>What You Get</h2>
		<ul>
			<li>Instructions for host</li>
			<li>Game rules</li>
			<li>Invitations to RSVP</li>
			<li>Character Assignment Invitations</li>
			<li>Name tag templates</li>
			<li>Character descriptions</li>
			<li>3 rounds of story for each character</li>
			<li>3 rounds of instructions for each character</li>
			<li>Tables for crime-solving notes</li>
		</ul>
	</div><!-- what-you-get -->
	<div class="block" data-tab="backstory">
		<h2>Backstory</h2>
		<?php echo $backStory ?>
	</div>
	<div class="block" data-tab="characters">
		<h2>Characters</h2>
		<?php
			$result = mysqli_query($con,"SELECT * FROM players");
			while($row = mysqli_fetch_array($result)) { ?>
				<h3 style="margin-bottom:0;" id='<?php echo $row['name'] ?>'><?php echo $row['name'] ?></h3>
				<p style="margin:0;">(<?php echo $row['role'] ?>)</p>
				<p style="margin:.5em 0;"><em><?php echo $row['description'] ?></em></p>
		<?php } ?>
	</div>
	<div class="pop" id="purchase-type">
		<div class="columns center">
			<div class="column small">
				<h3>Ala Carte</h3>
				<h4 style="margin-bottom:0;"><?php echo $title ?></h4>
				Price: $9.99
				<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover.jpg" style="width:100px; height:auto; display:block; margin:.5em auto;">
				<a href="/checkout?s2p-option=<?php echo $url ?>" class="button small" style="margin-bottom:1em;">Proceed to Checkout</a>
			</div>
			<div class="column cta small">
				<h3 style="margin-top:0;">Bulk Savings</h3>
				<p>Get 4 games for only $19.99!</p>
				<img src="<?php echo get_template_directory_uri() ?>/images/games/Crime and Prejudice/cover.jpg" style="width:100px;">
				<img src="<?php echo get_template_directory_uri() ?>/images/games/Deadly Reunion/cover.jpg" style="width:100px;">
				<img src="<?php echo get_template_directory_uri() ?>/images/games/Death Valley Villa/cover.jpg" style="width:100px;">
				<img src="<?php echo get_template_directory_uri() ?>/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" style="width:100px;">
				<div class="clear"></div>
				<a href="/bulk-savings" class="button small">Shop &amp; Save</a>
			</div>
		</div>
	</div><!-- pop -->
		<?php } ?>
		
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>
	<?php if ($category == 2) { ?>
		<footer class="entry-meta">
			<?php if ( comments_open() && ! is_single() ) : ?>
				<div class="comments-link">
					<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'twentythirteen' ) . '</span>', __( 'One comment so far', 'twentythirteen' ), __( 'View all % comments', 'twentythirteen' ) ); ?>
				</div><!-- .comments-link -->
			<?php endif; // comments_open() ?>
	
			<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
				<?php get_template_part( 'author-bio' ); ?>
			<?php endif; ?>
		</footer><!-- .entry-meta -->
	<?php } ?>
	<div class="clear"></div>
	</div>
</article><!-- #post -->
