// pop
function pop(url, data) {
	jQuery("body").prepend("<div class='pop'></div><div class='gray'></div>");
	jQuery(".gray, .pop").fadeIn(function() {
		jQuery(".gray").click(function() {
			jQuery(".gray, .pop").fadeOut(function() {
				jQuery(".gray").remove();
			});
		});
	});
	giver = data['giver'];
	receiver = data['receiver'];
	giverName = data['giverName'];
	receiverName = data['receiverName'];
	service = data['service'];
	jQuery(".pop").load(url, {
		giver: giver,
		receiver: receiver,
		giverName: giverName,
		receiverName: receiverName,
		service: service
	});
}
// end pop