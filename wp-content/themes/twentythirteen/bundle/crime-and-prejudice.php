<div id="content" class="site-content" role="main">

						
				
<article id="post-15" class="post-15 post type-post status-publish format-standard hentry category-store">
	<div class="bg"></div>
	<div class="content">
	<header class="entry-header">

				<h1 class="entry-title">
			Crime &amp; Prejudice			<small>Murder Mystery Party Game</small>
					</h1>
				
		<div class="entry-meta">
			<!--<span class="date"><a href="http://deadlyparties.com/store/crime-and-prejudice/" title="Permalink to Crime &amp; Prejudice" rel="bookmark"><time class="entry-date" datetime="2013-10-12T20:29:36+00:00">October 12, 2013</time></a></span><span class="categories-links"><a href="http://deadlyparties.com/category/store/" title="View all posts in Store" rel="category tag">Store</a></span><span class="author vcard"><a class="url fn n" href="http://deadlyparties.com/author/admin/" title="View all posts by admin" rel="author">admin</a></span>			<span class="edit-link"><a class="post-edit-link" href="http://deadlyparties.com/wp-admin/post.php?post=15&amp;action=edit">Edit</a></span>-->
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

		<div class="entry-content">
		
		<div style="display: none">UA:D [1.9.22_1171]</div>
<div class="ratingblock ">
<div class="ratingheader "></div>
<div class="ratingstars ">
<div id="article_rater_15" class="ratepost gdsr-oxygen gdsr-size-20">
<div class="starsbar gdsr-size-20">
<div class="gdouter gdheight">
<div id="gdr_vote_a15" style="width: 100px;" class="gdinner gdheight"></div>
</div>
</div>
</div>
</div>
<div class="ratingtext ">
<div id="gdr_text_a15" class="voted inactive">Rating: 5.0/<strong>5</strong> (3 votes cast)</div>
</div>
</div>
<p><span class="hreview-aggregate"><span class="item"><span class="fn">Crime &amp; Prejudice</span></span>, <span class="rating"><span class="average">5.0</span> out of <span class="best">5</span> based on <span class="votes">3</span> ratings <span class="summary"></span></span></span></p>
						<div class="column" style="position:relative;">
			<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Crime and Prejudice/cover.jpg" alt="Crime and Prejudice">
							<div class="discount">75%<br>Off</div>
					</div><!-- column -->
		<div class="column">
			<p style="margin-top:0;">The romantic world of Jane Austin's <em>Pride and Prejudice</em> has gotten a bit darker. In addition to complicated love triangles, the sudden death of the rich and respectable Anne de Bourgh has everyone pointing fingers. From lowly commoners like Elizabeth Bennet to high and mighty aristocrats like Fitzwilliam Darcy, no one's sure who trust ... let alone marry.</p>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Players:</th>
					<td>
						8					</td>
				</tr>
				<tr>
					<th>Gameplay:</th>
					<td>3 - 4 Hours</td>						
				</tr>
				<tr>
					<th>Ages:</th>
					<td>13 ↑</td>						
				</tr>
				<tr>
					<th>Rating:</th>
					<td>PG</td>						
				</tr>
				<tr>
					<th>Pages:</th>
					<td>≈ 65</td>						
				</tr>
			</tbody></table>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Formats:</th>
					<td>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf-50.png" alt="PDF" style="max-width:50px;">
						<div class="info" onclick="pop('pdf')">?</div>
						<div class="pop" id="pdf">
							<h3 style="margin-top:0;">Adobe PDF Format</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf.png" alt="PDF" style="max-width:140px;" class="alignleft">
							<p>If you prefer holding physical instructions, use the PDF format to make prints. The document also includes invitation templates, name tags, clues, and other resources that can be printed.</p>
						</div>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app-50.png" alt="web app" style="max-width:50px;">
						<div class="info" onclick="pop('web-app')">?</div>
						<div class="pop" id="web-app">
							<h3 style="margin-top:0;">Web App</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app.png" alt="web app" style="max-width:140px;" class="alignleft">
							<p>Use the web app for extra convenience and to cut down on printing costs. Each of your guests can access the game through any mobile device, laptop, or desktop.</p>
						</div>
					</td>
				</tr>
									<tr>
						<th>Retail:</th>
						<td>
							<div class="strike">$40.00<div class="line"></div></div>
						</td>
					</tr>
					<tr>
						<th>Your Price:</th>
						<td>
				    		$2.49<br>
							<small>($0.31/player)</small>
						</td>
					</tr>
							</tbody></table>
			<div class="clear"></div>
							<button onclick="pop('purchase-type')">Buy Now</button>
						<a href="/bulk-savings" class="button">Bulk Savings</a>
		</div><!-- column -->
		<div class="clear"></div>
		<br>
		

<ul class="tabs-menu">
	<li class="active gray" data-tab="details">Details</li>
	<li class="gray" data-tab="what-you-get">What You Get</li>
	<li class="gray" data-tab="backstory">Backstory</li>
	<li class="gray" data-tab="characters">Characters</li>
</ul>
<div class="clear"></div>
<div class="tabs-content">
	<div class="active block" data-tab="details">
		<h2>Details</h2>
		<ul>
			<li>Formats: Digital PDF / Web App</li>
			<li>Players:
				8				(4				males, 
				4 females)	
			</li>
			<li>Rating: PG</li>
			<li>Recommended Ages: 13 and up</li>
			<li>Approximate Pages: 65 single-sided / 33 double-sided</li>
			<li>Approximate Gameplay: 3 - 4 Hours</li>
			<li>Setting: Victorian England</li>
			<li>Author(s): Teresa and Stephen Gashler</li>
		</ul>
	</div><!-- details -->
	<div class="block" data-tab="what-you-get">
		<h2>What You Get</h2>
		<ul>
			<li>Instructions for host</li>
			<li>Game rules</li>
			<li>Invitations to RSVP</li>
			<li>Character Assignment Invitations</li>
			<li>Name tag templates</li>
			<li>Character descriptions</li>
			<li>3 rounds of story for each character</li>
			<li>3 rounds of instructions for each character</li>
			<li>Tables for crime-solving notes</li>
		</ul>
	</div><!-- what-you-get -->
	<div class="block" data-tab="backstory">
		<h2>Backstory</h2>
		<p>Though the venerable Lady Catherine de Bourgh, one of the richest women in England, was certain that she had only invited the most fashionable, renowned, and wealthy aristocrats to her dinner party, there were rumors that certain commoners had weaseled their way among her respectable society. Not the least troubling of which was Elizabeth Bennet, that loud-mouthed usurper of class, who wouldn't keep her lowly eyes off of Catherine's nephew Fitzwilliam Darcy. Catherine had done everything in her power to persuade Darcy to marry a much more suitable woman: her daughter Anne, but Darcy was as thick-skulled as Elizabeth.</p><p>Other guests complained about the appearance of the scandalous soldier George Wickham, who had robbed many girls' hearts before running off with their money. Though no one could prove these rumors, everyone was certain that something mischievous was going on in the de Bourgh mansion ... especially when, following a toast, someone screamed with terror, there was a gunshot, and Anne de Bourgh was found lying with her face on her plate.</p><p>As soon as Anne was pronounced dead, Lady Catherine, not wanting any tampering with the evidence, ordered everyone out of the dining hall until the authorities arrived, who eventually took away the body for autopsy. In the mean time, no one seemed sure of what, exactly, had happened.</p><p>Was it an accident or a murder? If the latter, who could have committed such a nefarious deed? Was it the jealousy of the Bennet family, whose lowly class was forbidden in the de Bourgh mansion? Was it the retaliation of the heartless Mr. Darcy against the girl he wanted nothing to do with? Perhaps it was the reckless action of Darcy's friend Charles Bingley, who, as rumor reported, had recently taken to drinking and had been known to lash out. Could it have been the greedy Mr. Collins, who was rumored to have been brown-nosing Lady Catherine, perhaps lusting after her luxurious mansion and eager to remove competition for inheritance? Or was it possible that the villainous Mr. Wickham was in their midst after all, trying to put an end to the rumors spread by Anne, one of his many ex lovers?</p><p>The next day, Lady Catherine ordered everyone whom she suspected to her mansion ... even the unworthy Bennet girls. As everyone was afraid of the old woman, no one dared disobey. And that is why you are all here. Each one of you has been ordered to recount your dealings of yesterday, stating exactly where you were, what you were doing, and revealing any information that could lead to an explanation of Anne's mysterious death. Begin doing so now.</p>	</div>
	<div class="block" data-tab="characters">
		<h2>Characters</h2>
						<h3 style="margin-bottom:0;" id="Elizabeth Bennet">Elizabeth Bennet</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a clever and independent single woman who can't stand social proprieties among the rich. Your sisters are Jane and Lydia.</em></p>
						<h3 style="margin-bottom:0;" id="Fitzwilliam Darcy">Fitzwilliam Darcy</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a wealthy single gentleman who is in love with Elizabeth Bennet. You're brother to Georgiana, nephew to Lady Catherine de Bourgh, and cousin to Anne de Bourgh.</em></p>
						<h3 style="margin-bottom:0;" id="Jane Bennet">Jane Bennet</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a beautiful single woman with a sweet and innocent personality, sister of Elizabeth and Lydia.</em></p>
						<h3 style="margin-bottom:0;" id="Charles Bingley">Charles Bingley</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're rich, good natured, but not too bright. Your best friend is Fitzwilliam Darcy.<br><br></em>Costume Notes</em><br>Keep a flask of booze in your pocket.</em></p>
						<h3 style="margin-bottom:0;" id="Lydia Bennet">Lydia Bennet</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a silly and boy crazy young woman, sister to Elizabeth and Jane.</em></p>
						<h3 style="margin-bottom:0;" id="George Wickham">George Wickham</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a handsome, dashing soldier whom the girls want. Unfortunately, it's hard for you to hang onto your money.</em></p>
						<h3 style="margin-bottom:0;" id="Georgiana Darcy">Georgiana Darcy</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a shy but accomplished young woman, sister to Fitzwilliam, niece to Lady Catherine de Bourgh, and cousin to Anne de Bourgh.</em></p>
						<h3 style="margin-bottom:0;" id="William Collins">William Collins</h3>
				<p style="margin:0;"></p>
				<p style="margin:.5em 0;"><em>You're a socially awkward priest under the patronage of Lady Catherine de Bourgh, also a distant cousin of the Bennets.</em></p>
			</div>
	<div class="pop" id="purchase-type">
		<div class="columns center">
			<div class="column small">
				<h3>Ala Carte</h3>
				<h4 style="margin-bottom:0;">Crime and Prejudice</h4>
				Price: $4.99
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Crime and Prejudice/cover.jpg" style="width:100px; height:auto; display:block; margin:.5em auto;">
				<a href="/checkout?s2p-option=crime-and-prejudice" class="button small" style="margin-bottom:1em;">Proceed to Checkout</a>
			</div>
			<div class="column cta small">
				<h3 style="margin-top:0;">Bulk Savings</h3>
				<p>Get 4 games for only $10!</p>
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Crime and Prejudice/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Deadly Reunion/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Death Valley Villa/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" style="width:100px;">
				<div class="clear"></div>
				<a href="/bulk-savings" class="button small">Shop &amp; Save</a>
			</div>
		</div>
	</div><!-- pop -->
				
			</div><!-- .entry-content -->
			<div class="clear"></div>
	</div>
</div></article><!-- #post -->
			<h2>Reviews</h2>				
<div id="comments" class="comments-area">

			<h2 class="comments-title">
			3 thoughts on “<span>Crime &amp; Prejudice</span>”		</h2>

		<ol class="comment-list">
					<li id="comment-6" class="comment even thread-even depth-1">
			<article id="div-comment-6" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/bd4c9a4b92be466089ec5ac093ba4082?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">Jennifer Blake</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/crime-and-prejudice/#comment-6">
							<time datetime="2013-11-24T04:28:07+00:00">
								November 24, 2013 at 4:28 am							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=6">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>I had a blast playing this game!  The authors did a great job in creating suspense all around such that it wasn’t obvious who committed the murder until all the information had been shared.  Having the characters based off a familiar story helped the actors in my group easily relate to and act as the characters would have acted.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/crime-and-prejudice/?replytocom=6#respond" onclick="return addComment.moveForm(&quot;div-comment-6&quot;, &quot;6&quot;, &quot;respond&quot;, &quot;15&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-10" class="comment odd alt thread-odd thread-alt depth-1">
			<article id="div-comment-10" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://0.gravatar.com/avatar/667351c7c93d9f74674ec988a733122d?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">Judy R. Field</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/crime-and-prejudice/#comment-10">
							<time datetime="2013-11-27T19:14:12+00:00">
								November 27, 2013 at 7:14 pm							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=10">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Loved this! A must-play for Austen fans. While reading the book and watching the movies will always have a place in my heart, there’s nothing like acting out the roles with your friends. I didn’t mind that the authors took quite a few liberties to bring us an alternate Pride and Prejudice world. It totally worked.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/crime-and-prejudice/?replytocom=10#respond" onclick="return addComment.moveForm(&quot;div-comment-10&quot;, &quot;10&quot;, &quot;respond&quot;, &quot;15&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-13" class="comment even thread-even depth-1">
			<article id="div-comment-13" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/98a399ab0f7ce13666fab1dda089bce6?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">faerygirl55</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/crime-and-prejudice/#comment-13">
							<time datetime="2013-11-27T19:34:21+00:00">
								November 27, 2013 at 7:34 pm							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=13">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Recently hosted this game. Some players were reluctant at first, especially my friend assigned to be Mr. Collins (I can’t blame him because who wants to be Mr. Collins?). As the players really got into the first round and completed their actions, it turned out to be a hilarious game! And who turned out being the most memorable character? Mr. Collins. Each character had a fun personality and good instructions on what to do. We laughed all night.</p>
<p>Just to warn, this game does take liberties. It alludes to scenarios that don’t actually happen in the book. And it makes Mr. Bingley a drunk. And it seems that you don’t actually have to be familiar with Pride and Prejudice beforehand to understand this game. Which worked well for our game, because we had some players who knew the book and some who did not.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/crime-and-prejudice/?replytocom=13#respond" onclick="return addComment.moveForm(&quot;div-comment-13&quot;, &quot;13&quot;, &quot;respond&quot;, &quot;15&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		</ol><!-- .comment-list -->

		
		
	
									<div id="respond" class="comment-respond">
				<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/store/crime-and-prejudice/#respond" style="display:none;">Cancel reply</a></small></h3>
									<form action="http://deadlyparties.com/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
																			<p class="logged-in-as">Logged in as <a href="http://deadlyparties.com/wp-admin/profile.php">admin</a>. <a href="http://deadlyparties.com/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fdeadlyparties.com%2Fstore%2Fcrime-and-prejudice%2F&amp;_wpnonce=6bd26a3463" title="Log out of this account">Log out?</a></p>																			<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>						<p class="form-allowed-tags">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </code></p>						<p class="form-submit">
							<input name="submit" type="submit" id="submit" value="Post Comment">
							<input type="hidden" name="comment_post_ID" value="15" id="comment_post_ID">
<input type="hidden" name="comment_parent" id="comment_parent" value="0">
						</p>
						<input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment" value="62ee716720"><script>(function{if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}});</script>
<p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="c541627d46"></p>					</form>
							</div><!-- #respond -->
			
</div><!-- #comments -->								</div>