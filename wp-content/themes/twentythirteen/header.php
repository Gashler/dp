<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-45150719-1', 'deadlyparties.com');
	  ga('send', 'pageview');
	
	</script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href="<?php echo get_template_directory_uri() ?>/css/site.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script src="<?php echo get_template_directory_uri() ?>/js/functions.js"></script>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner" style="position:relative; display:block;">
			<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<div id="home-link-text">
					<?php if (is_front_page()) { ?>
						<h1 style="margin-bottom:0;">
					<?php } ?>
					<img src="<?php echo get_bloginfo('template_url') ?>/images/logo.png" alt="<?php bloginfo( 'description' ); ?>">
					<?php if (is_front_page()) { ?>
						</h1>
					<?php } ?>
				</div>
				<?php if (is_front_page()) { ?>
					<img src="<?php echo get_bloginfo('template_url') ?>/images/seals.png" alt="Wholesale Discounts | Satisfaction Guarantee" id="home-seals" width="150">
					<img src="<?php echo get_bloginfo('template_url') ?>/images/checkout-seals.png" width="255" height="98" id="home-symbols">
				<?php } else { ?>
					<img src="<?php echo get_bloginfo('template_url') ?>/images/seals.png" alt="Wholesale Discounts | Satisfaction Guarantee" id="header-seals" width="150">
					<img src="<?php echo get_bloginfo('template_url') ?>/images/checkout-seals.png" width="255" height="98" id="header-symbols">
				<?php } ?>
			</a>
			<div class="clear"></div>
<?php if (!(is_page(404))) { ?>
			<div id="navbar" class="navbar gray">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<li id="menu-button"><a href="#"><img src="<?php echo get_bloginfo('template_url') ?>/images/menu.png" /></a></li>
					<?php get_search_form(); ?>
										<div class="clear"></div>
					<div class="menu">
						<li><a href="/index.php">Home</a></li>
						<li><a href="/about">About</a></li>
						<li><a href="/category/store">Games</a></li>
						<li><a href="/bulk-savings">Bulk Savings</a></li>
						<li><a href="/how-to-play">How to Play</a></li>
						<li><a href="/free-stuff">Free Stuff</a></li>
						<li><a href="/category/party-ideas">Party Ideas</a></li>
					</div>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
<?php } ?>
		</header><!-- #masthead -->
		<div id="main" class="site-main">
<?php if (is_page(404) || is_page(476)) { ?>
	<div id="home-hero-container" style="box-shadow:0 0 100px rgb(96,96,96);">
		<div id="home-hero-offer-banner">Best value online or in stores!</div>
		<img id="home-hero-offer" src="<?php echo get_template_directory_uri() ?>/images/bundle-hero-offer.png" alt="Get 4 Games for Only $49.99">
		<div id="home-hero-caption">
			<div class="bg dark"></div>
			<h2>
				<img src="<?php echo get_template_directory_uri() ?>/images/host-your-own-murder-mystery.png" alt="Host Your Own Murder Mystery">
			</h2>
			<!--<a href="/bulk-savings" class="button">Buy Now</a>-->
		</div>
		<div id="home-hero-icons">
			<img src="<?php echo get_template_directory_uri() ?>/images/mediums.png" width="225" alt="Download PDF or play online web app">
		</div>
		<img id="home-hero" src="<?php echo get_template_directory_uri() ?>/images/home-hero-2.jpg">
	</div>	
<?php } ?>