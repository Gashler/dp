<h2 style="color:red;">Normally $160.00 (Save $150.00!)</h2>
<div class="bundle">
	<div class="column">
		<p>
			<img src="<?php echo get_template_directory_uri() ?>/images/limited-time-offer.jpg" width="150" height="150" style="width:150px; height:150px; box-shadow:none; margin:0 0 1em 1em; float:right;" alt="Ultimate Savings limited time offer">
			<strong>Host 4 of your own murder mystery parties</strong> with these top-rated games at an unbeatable price. With up to 16 hours of laugh-out-loud, crime-solving, bone-chilling fun, you won't find a better deal at any other store. Comparable to the <em>How to Host a Murder&trade;</em> game series, DeadlyParties.com games provide 3 rounds of role-playing scripts for multiple players.
		</p>
		<p>
			<strong>The object of the game</strong> is to solve the murder (or get away with the crime). Simply download and print the games for instant access. Unlike any other series, Deadlyparties.com games can also be played on mobile devices for a paper-free environment. Not convinced yet? Read the reviews below to see what others are saying.
		</p>
		<ul>
			<li><strong>Best deal</strong> online or in stores</li>
			<li><strong>Top-rated</strong> murder mystery games</li>
			<li><strong>Up to 16 hours</strong> of gameplay</li>
			<li><strong>No shipping costs</strong> or waiting (simply print PDF's)</li>
			<li><strong>Play cloud-based</strong> versions on mobile devices</li>
			<li><strong>Invitations</strong>, name tags, clues, scripts, and more</li>
			<li><strong>100% satisfaction</strong> guaranteed</li>
		</ul>
	<p onclick="pop('about')"><a href="#">Click here to learn more about DeadlyParties.com games.</a></p>
	<div class="pop" id="about">
		<h2>About DeadlyParties.com</h2>
		<p><a href="http://deadlyparties.com/wp-content/uploads/2013/10/1370768_78739444.jpg"><img class="alignright size-medium wp-image-218" alt="1370768_78739444" src="http://deadlyparties.com/wp-content/uploads/2013/10/1370768_78739444-300x300.jpg" height="200" width="200" style="width:100%; max-width:300px; max-height:300px;"></a>DeadylParties.com games (comparable to the How to Host a Murder Mystery™ games) involve socialization, storytelling, role-playing, puzzle-solving, competition, and lots of suspense. Whether you enjoy acting up or prefer to keep to yourself, there’s a character for everyone.</p>
<p>Rather than paying extra for shipping and waiting for your game to arrive, DeadylParties.com games can be instantly downloaded and printed. There’s no better solution for a suspense-filled, challenging, laugh-out-loud evening with friends. Stephen and Teresa Gashler, the writers behind DeadlyParties.com, have been hosting mystery parties since 1999. They’ve won multiple awards for their novels, plays, screenplays, and storytelling. Having created myriads of mind-blowing puzzles, back-stabbing characters, and bone-chilling thrills, they know the art of fun.</p>
	</div>
	</div><!-- column -->
	<div class="column" style="position:relative; overflow:visible;">
		<img src="<?php echo get_template_directory_uri() ?>/images/sale-ends-today.png" alt="Sale Ends Today" style="position:absolute; box-shadow:none; width:200px; right:-100px; top:-13px;">
		<table class="gray" width="100%">
			<thead>
				<tr>
				<th>Games Included</th>
				<th>Price</th>
				</tr>
			</thead>
			<tbody>
				<tr>
				<th>Crime and Prejudice<br><small></th>
				<td><div class="strike">$40.00<div class="line"></div></div> $2<small style="font-size:8pt; display:inline-block; position:relative; top:-4px;">.49</small></td>
				</tr>
				<tr>
				<th>Deadly Reunion</th>
				<td><div class="strike">$40.00<div class="line"></div></div> $2<small style="font-size:8pt; display:inline-block; position:relative; top:-4px;">.49</small></td>
				</tr>
				<tr>
				<th>Death Valley Villa</th>
				<td><div class="strike">$40.00<div class="line"></div></div> $2<small style="font-size:8pt; display:inline-block; position:relative; top:-4px;">.49</small></td>
				</tr>
				<tr>
				<th>Mary Rotter</th>
				<td><div class="strike">$40.00<div class="line"></div></div> $2<small style="font-size:8pt; display:inline-block; position:relative; top:-4px;">.49</small></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
				<th style="font-weight:100;">Taxes:</th>
				<td><div class="strike">$10.80<div class="line"></div></div> $0<small style="font-size:8pt; display:inline-block; position:relative; top:-4px;">.00</small></td>
				</tr>
				<tr>
				<th>Total:</th>
				<td style="font-weight:bold;"><div class="strike">$160.00<div class="line"></div></div> $9<small style="font-size:8pt; display:inline-block; position:relative; top:-4px;">.99</small></td>
				</tr>
			</tfoot>
		</table>
		<a href="/bulk-savings-checkout" class="button" style="display:block; text-align:center; font-size:2em;">Buy Now | Save Big</a>
	</div>
	<div class="clear"></div>
<h2 style="margin-bottom:.5em;">The Games</h2>
	<ul class="tabs-menu">
		<li data-tab="crime-and-prejudice" class="active">Crime and Prejudice</li>
		<li data-tab="death-valley-villa">Death Valley Villa</li>
		<li data-tab="deadly-reunion">Deadly Reunion</li>
		<li data-tab="mary-rotter">Mary Rotter</li>
	</ul>
	<div class="tabs-content">
		<div class="block active" data-tab="crime-and-prejudice">
			<?php include "wp-content/themes/twentythirteen/bundle/crime-and-prejudice.php" ?>
		</div><!-- block -->
		<div class="block" data-tab="death-valley-villa">
			<?php include "wp-content/themes/twentythirteen/bundle/death-valley-villa.php" ?>
		</div><!-- block -->
		<div class="block" data-tab="deadly-reunion">
			<?php include "wp-content/themes/twentythirteen/bundle/deadly-reunion.php" ?>
		</div><!-- block -->
		<div class="block" data-tab="mary-rotter">
			<?php include "wp-content/themes/twentythirteen/bundle/mary-rotter.php" ?>
		</div><!-- block -->
	</div><!-- tabs-content -->
	<a href="/bulk-savings-checkout" class="button" style="float:right; text-align:center; font-size:2em;">Buy Now | Save Big</a>
	<div class="clear"></div>
	<script>
		var data_id = 1;
		var set = 1;
		jQuery(".tabs-menu, .tabs-content").each(function() {
			jQuery(this).attr("data-id", data_id);
			if (set == 2) {
				set = 1;
				data_id ++;
			}
			else set ++;
		});
	</script>
</div>