<?php
	
	// edit database
	
	$database = $_POST['database'];
	
	// create database
	if ($_POST['new_database']) {
		$con = mysqli_connect("localhost","root","Penguin4Shpenguin!?");
		// Check connection
		if (mysqli_connect_errno()) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
		// Create database
		$sql = "CREATE DATABASE $_POST[new_database]";
		if (mysqli_query($con,$sql)) {
			echo "Created $_POST[new_database]";
		}
		else {
			echo "Error creating database: " . mysqli_error($con);
		}
		
		// Create table
		$con = mysqli_connect("localhost","root","Penguin4Shpenguin!?",$_POST['new_database']);
		$sql = "CREATE TABLE game(title TEXT, author TEXT, description TEXT, rating TEXT, setting TEXT, back_story TEXT, epilogue TEXT)";
		
		// Execute query
		if (mysqli_query($con,$sql)) {
			echo "Table game created successfully";
		}
		else {
			echo "Error creating table: " . mysqli_error($con);
		}
		$sql = "CREATE TABLE players(name TEXT, sex TEXT, description TEXT, round1_story TEXT, round1_action TEXT, round2_story TEXT, round2_action TEXT, round3_story TEXT, round3_action TEXT)";
		// Execute query
		if (mysqli_query($con,$sql)) {
			echo "Table game created successfully";
		}
		else {
			echo "Error creating table: " . mysqli_error($con);
		}

		// add rows
		$sql="INSERT INTO game (title)
		VALUES
		('New Game')";
		
		if (!mysqli_query($con,$sql))
		  {
		  die('Error: ' . mysqli_error($con));
		  }
		  
		$sql="INSERT INTO players (name)
		VALUES
		('New Character')";
		
		if (!mysqli_query($con,$sql))
		  {
		  die('Error: ' . mysqli_error($con));
		  }
		
		mysqli_close($con);
	}
	
?>
<div id="load" style="display:none;"></div>
<form action="/editor" method="post" style="float:left; margin-right:1em;">
	<label for="databases">Edit Game:</label>
	<select id="databases" name="database" style="margin-bottom:1em;">
		<?php
			$result = mysql_query("SHOW DATABASES LIKE 'deadlyp\_%'");
			while ($row = mysql_fetch_array($result)) {
			    echo "<option>" . $row[0]. "</option>";
			}
		?>
	</select>
	<button class="tiny gray">Edit Game</button>
</form>
<form action="/editor" method="post" style="float:left;">
	<label for="new_database">New Game:</label>
	<input type="text" id="new_database" name="new_database">
	<button class="tiny gray">Create Game</button>
</form>
<div class="clear"></div>
<?php if ($database !== null) { ?>
	<ul class="tabs-menu">
		<li data-tab="game" class="active">Game</li>
		<li data-tab="players">Players</li>
	</ul>
	<div class="tabs-content">
		<div class="block active" data-tab="game" style="overflow:hidden;">
			<?php
				$table = "game";
				include "wp-content/themes/twentythirteen/editor/editor.php"
			?>
		</div>
		<div class="block" data-tab="players" style="overflow:hidden;">
			<?php
				$table = "players";
				include "wp-content/themes/twentythirteen/editor/editor.php"
			?>
		</div>
	</div><!-- tabs-content -->
<?php } ?>
<script>

	// choose database
	
	jQuery("#databases").change(function() {
		var database = jQuery(this).value();
	});
	jQuery()

	// load content

	jQuery(".block option").click(function() {
		var table = jQuery(this).parents(".block").attr("data-tab");
		var database = "<?php echo $database ?>";
		if (table == "players") {
			var field = jQuery("#players-fields").val();
			name = jQuery("#names").val();
			jQuery("#load").load("/wp-content/themes/twentythirteen/editor/load-content.php", {
				database: database,
				table: table,
				field: field,
				name: name
			});
		}
		else {
			var field = jQuery(this).html();
			jQuery("#load").load("/wp-content/themes/twentythirteen/editor/load-content.php", {
				database: database,
				table: table,
				field: field
			});
		}
		var value = jQuery("#load").html();
		jQuery("#load").html("");
		jQuery("#" + table + "-content").val(value);
	});
	
	// update content
	
	jQuery("textarea").keyup(function() {
		var database = "<?php echo $database ?>";
		var table = jQuery(this).parents(".block").attr("data-tab");
		var field = jQuery("#" + table + "-fields").val();
		var value = jQuery(this).val();
		if (table == "players") {
			name = jQuery("#names").val();
			if (jQuery("#players-fields").val() == "name") {
				jQuery("#names option:selected").html(value);
			}
			jQuery("#load").load("/wp-content/themes/twentythirteen/editor/update-content.php", {
				database: database,
				table: table,
				field: field,
				value: value,
				name: name
			});
		}
		else {
			jQuery("#load").load("/wp-content/themes/twentythirteen/editor/update-content.php", {
				database: database,
				table: table,
				field: field,
				value: value
			});
		}
	});
	
	// add row
	
	jQuery("#add_row").click(function() {
		var database = "<?php echo $database ?>";
		jQuery("#load").load("/wp-content/themes/twentythirteen/editor/addRow.php", {
			database: database
		}, function() {
			jQuery("#names").append("<option>New Character</option>");
		});
	});
</script>