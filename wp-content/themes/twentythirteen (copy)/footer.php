<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer gray" role="contentinfo">
			<?php get_sidebar( 'main' ); ?>

			<div class="site-info">
				<a href="/index.php">Home</a> &nbsp;|&nbsp;
				<a href="/about">About</a> &nbsp;|&nbsp;
				<a href="/customer-service">Contact</a><br><br>
				Copyright &copy; <?php echo date('Y') ?>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>