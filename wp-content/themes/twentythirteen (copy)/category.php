<?php
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="bg"></div>
		<div id="content" class="site-content tiles" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( '%s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<!--<div class="archive-meta"><?php echo category_description(); ?></div>-->
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="position:relative;">
						<div class="bg"></div>
						<?php
							$category = get_the_category(); 
							$category = $category[0]->cat_name;
							if ($category == "Store") {
								$discount = get_field('discount');
								if ($discount !== "0") {
						?>
							<div class="discount" style="z-index:1;"><?php the_field('discount') ?>%<br>Off</div>
						<?php
								}
							}
						?>
						<div class="content">
						<header class="entry-header">
							<h2 class="entry-title">
								<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h2>
							<div class="entry-content">
								<?php
									if ($category == "Store") {
								?>
								<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
								<?php } else { ?>
									<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>			
										<a href="<?php the_permalink(); ?>" rel="bookmark">
											<div class="entry-thumbnail alignleft">
												<?php the_post_thumbnail(); ?>
											</div>
										</a>
									<?php endif ?>
								<?php } ?>
								<?php
									$game_db = get_field('game_db');
									$download_id = get_field('download_id');
									$con=mysqli_connect("localhost","root","Penguin4Shpenguin!?",$game_db);
									$result = mysqli_query($con,"SELECT * FROM game");
									while($row = mysqli_fetch_array($result)) {
										$title = $row['title'];
										$author = $row['author'];
										$description = $row['description'];
										$rating = $row['rating'];
										$pages = $row['pages'];
										$backStory = $row['back_story'];
									}
								?>
							<a href="<?php the_permalink(); ?>">
							<?php if ($category !== "Party Ideas") { ?>
								<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover-small.jpg" alt="<?php echo $title ?>" class="alignleft">
							<?php } ?>
							</a>
							</div><!-- entry-content -->
							<div class="entry-meta">
								<!--<?php twentythirteen_entry_meta(); ?>
								<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>-->
							</div><!-- .entry-meta -->
						</header><!-- .entry-header -->
					
						<?php if ( is_search() ) : // Only display Excerpts for Search ?>
						<div class="entry-summary">
							<?php the_excerpt(); ?>
						</div><!-- .entry-summary -->
						<?php endif; ?>
						<div style="text-align:center">
							<?php if ($category == "Store") { ?>
								<a class="button center small" href="<?php the_permalink(); ?>" rel="bookmark">View Game</a>
							<?php } else { ?>
								<a class="button center small" href="<?php the_permalink(); ?>" rel="bookmark">View Article</a>								
							<?php } ?>
						</div>
						<footer class="entry-meta">
							<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
								<?php get_template_part( 'author-bio' ); ?>
							<?php endif; ?>
						</footer><!-- .entry-meta -->
						<div class="clear"></div>
						</div>
					</article><!-- #post -->
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>






		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>