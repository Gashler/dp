<div id="content" class="site-content" role="main">

						
				
<article id="post-36" class="post-36 post type-post status-publish format-standard hentry category-store">
	<div class="bg"></div>
	<div class="content">
	<header class="entry-header">

				<h1 class="entry-title">
			Death Valley Villa			<small>Murder Mystery Party Game</small>
					</h1>
				
		<div class="entry-meta">
			<!--<span class="date"><a href="http://deadlyparties.com/store/death-valley-villa/" title="Permalink to Death Valley Villa" rel="bookmark"><time class="entry-date" datetime="2013-10-19T00:33:08+00:00">October 19, 2013</time></a></span><span class="categories-links"><a href="http://deadlyparties.com/category/store/" title="View all posts in Store" rel="category tag">Store</a></span><span class="author vcard"><a class="url fn n" href="http://deadlyparties.com/author/admin/" title="View all posts by admin" rel="author">admin</a></span>			-->
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

		<div class="entry-content">
		
		<div style="display: none">VN:D [1.9.22_1171]</div>
<div class="ratingblock ">
<div class="ratingheader "></div>
<div class="ratingstars ">
<div id="article_rater_36" class="ratepost gdsr-oxygen gdsr-size-20">
<div class="starsbar gdsr-size-20">
<div class="gdouter gdheight">
<div id="gdr_vote_a36" style="width: 100px;" class="gdinner gdheight"></div>
</div>
</div>
</div>
</div>
<div class="ratingtext ">
<div id="gdr_text_a36" class="voted inactive">Rating: 5.0/<strong>5</strong> (4 votes cast)</div>
</div>
</div>
<p><span class="hreview-aggregate"><span class="item"><span class="fn">Death Valley Villa</span></span>, <span class="rating"><span class="average">5.0</span> out of <span class="best">5</span> based on <span class="votes">4</span> ratings <span class="summary"></span></span></span></p>
						<div class="column" style="position:relative;">
			<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Death Valley Villa/cover.jpg" alt="Death Valley Villa">
							<div class="discount">75%<br>Off</div>
					</div><!-- column -->
		<div class="column">
			<p style="margin-top:0;">Enter the wild, wild west with a gun-slinging gaucho, a sharp-shootin' sheriff, a wily saloon girl, and enough gold nuggets to send the whole town into an uproar. See if you can solve the case of the murdered preacher, but don't rub up against the wrong company, or you might find yourself in a teeth-grittin' duel.</p>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Players:</th>
					<td>
						8					</td>
				</tr>
				<tr>
					<th>Gameplay:</th>
					<td>3 - 4 Hours</td>						
				</tr>
				<tr>
					<th>Ages:</th>
					<td>13 ↑</td>						
				</tr>
				<tr>
					<th>Rating:</th>
					<td>PG-13</td>						
				</tr>
				<tr>
					<th>Pages:</th>
					<td>≈ 65</td>						
				</tr>
			</tbody></table>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Formats:</th>
					<td>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf-50.png" alt="PDF" style="max-width:50px;">
						<div class="info" onclick="pop('pdf')">?</div>
						<div class="pop" id="pdf">
							<h3 style="margin-top:0;">Adobe PDF Format</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf.png" alt="PDF" style="max-width:140px;" class="alignleft">
							<p>If you prefer holding physical instructions, use the PDF format to make prints. The document also includes invitation templates, name tags, clues, and other resources that can be printed.</p>
						</div>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app-50.png" alt="web app" style="max-width:50px;">
						<div class="info" onclick="pop('web-app')">?</div>
						<div class="pop" id="web-app">
							<h3 style="margin-top:0;">Web App</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app.png" alt="web app" style="max-width:140px;" class="alignleft">
							<p>Use the web app for extra convenience and to cut down on printing costs. Each of your guests can access the game through any mobile device, laptop, or desktop.</p>
						</div>
					</td>
				</tr>
									<tr>
						<th>Retail:</th>
						<td>
							<div class="strike">$40.00<div class="line"></div></div>
						</td>
					</tr>
					<tr>
						<th>Your Price:</th>
						<td>
				    $2.49<br>
							<small>($0.31/player)</small>
						</td>
					</tr>
							</tbody></table>
			<div class="clear"></div>
							<button onclick="pop('purchase-type')">Buy Now</button>
						<a href="/bulk-savings" class="button">Bulk Savings</a>
		</div><!-- column -->
		<div class="clear"></div>
		<br>
		

<ul class="tabs-menu">
	<li class="active gray" data-tab="details">Details</li>
	<li class="gray" data-tab="what-you-get">What You Get</li>
	<li class="gray" data-tab="backstory">Backstory</li>
	<li class="gray" data-tab="characters">Characters</li>
</ul>
<div class="clear"></div>
<div class="tabs-content">
	<div class="active block" data-tab="details">
		<h2>Details</h2>
		<ul>
			<li>Formats: Digital PDF / Web App</li>
			<li>Players:
				8				(4				males, 
				4 females)	
			</li>
			<li>Rating: PG-13</li>
			<li>Recommended Ages: 13 and up</li>
			<li>Approximate Pages: 65 single-sided / 33 double-sided</li>
			<li>Approximate Gameplay: 3 - 4 Hours</li>
			<li>Setting: Nineteenth Century Cowboy Western</li>
			<li>Author(s): Stephen Gashler</li>
		</ul>
	</div><!-- details -->
	<div class="block" data-tab="what-you-get">
		<h2>What You Get</h2>
		<ul>
			<li>Instructions for host</li>
			<li>Game rules</li>
			<li>Invitations to RSVP</li>
			<li>Character Assignment Invitations</li>
			<li>Name tag templates</li>
			<li>Character descriptions</li>
			<li>3 rounds of story for each character</li>
			<li>3 rounds of instructions for each character</li>
			<li>Tables for crime-solving notes</li>
		</ul>
	</div><!-- what-you-get -->
	<div class="block" data-tab="backstory">
		<h2>Backstory</h2>
		<p>It was an average Monday evening in Death Valley Villa, California, until the church bell started ringing. By the time Sheriff Dustina Judson and Ella Scarberry, the farmer's wife, had arrived on the scene, the bell had stopped ringing. The preacher, Justice Stimpson, lay dead on the front steps of the church, a bullet hole in the back of his head. Apparently he had been walking down the steps when someone had shot him, and he'd fallen onto his face. Across the street, Georgie Ackerson, the town drunk, was lying next to a tree, fast asleep. In his hand was a revolver. As the rest of the town gathered around, the sheriff asked, "Who was ringing the bell?" But no one had an answer.</p><p>It was a well-known fact that the preacher wasn't exactly in good standing with the town. He'd managed to offend nearly everyone with his cries for repentance and threats of hellfire. Though it was hard to imagine that someone in their midst would have gone so far as to kill the old man, with the closest town being over one-hundred miles away, no one else could have done it.</p><p>The next morning, the sheriff rounded up everyone she suspected and brought them to the saloon (the closest thing in Death Valley Villa to a meeting hall). There they found the newcomer to town, the mysterious gaucho, hiding in the upstairs bedroom. He was forced to sit with the others. "Good," said the sheriff, "I was hoping you could join us." Then she said to everyone,  "I've brought you all here because each and everyone of you have been acting fishy in the last few days. Take a seat and get comfortable, because one of us is a murderer, and the rest of us aren't going to be able to sleep until we find out who. From this moment on, there can't be any secrets between us. Now I know that most of you already know each other, but for the sake of ..." All eyes turned to the gaucho. "For our new friend over there," she continued, "I want everyone of you to state your name and tell us about yourself."</p>	</div>
	<div class="block" data-tab="characters">
		<h2>Characters</h2>
						<h3 style="margin-bottom:0;" id="Dustina Judson">Dustina Judson</h3>
				<p style="margin:0;">(sheriff)</p>
				<p style="margin:.5em 0;"><em>A no-nonsense, gun-slinging cowgirl, as tough as her leather britches</em></p>
						<h3 style="margin-bottom:0;" id="Alanso Don Fuego">Alanso Don Fuego</h3>
				<p style="margin:0;">(gaucho)</p>
				<p style="margin:.5em 0;"><em>A dashingly good-looking specimen from South of the border, and, according to rumor, a bit of a bad boy</em></p>
						<h3 style="margin-bottom:0;" id="Pansy Purvis">Pansy Purvis</h3>
				<p style="margin:0;">(saloon girl)</p>
				<p style="margin:.5em 0;"><em>The type of risque beauty any respectable mother would tell her sons to stay away from</em></p>
						<h3 style="margin-bottom:0;" id="Bernadine Stimpson">Bernadine Stimpson</h3>
				<p style="margin:0;">(preacher's daughter)</p>
				<p style="margin:.5em 0;"><em>Sweet, young, and innocent with a bottled-up spark of rebellion</em></p>
						<h3 style="margin-bottom:0;" id="Ella Scarberry">Ella Scarberry</h3>
				<p style="margin:0;">(farmer's wife)</p>
				<p style="margin:.5em 0;"><em>The best-looking woman in town, whose noble air deserved better than her humble circumstances</em></p>
						<h3 style="margin-bottom:0;" id="Frankie Barnhard">Frankie Barnhard</h3>
				<p style="margin:0;">(cowboy)</p>
				<p style="margin:.5em 0;"><em>An all-American, corn-fed hero of a man, though perhaps a bit lacking in intelligence</em></p>
						<h3 style="margin-bottom:0;" id="Georgie Ackerson">Georgie Ackerson</h3>
				<p style="margin:0;">(town drunk)</p>
				<p style="margin:.5em 0;"><em>A kind, yet misunderstood young man with prematurely shattered dreams ... a tragic victim of society</em></p>
						<h3 style="margin-bottom:0;" id="Bronco Dalton">Bronco Dalton</h3>
				<p style="margin:0;">(bartender)</p>
				<p style="margin:.5em 0;"><em>The very face of grizzly, old sin ... and not ashamed of it</em></p>
			</div>
	<div class="pop" id="purchase-type">
		<div class="columns center">
			<div class="column small">
				<h3>Ala Carte</h3>
				<h4 style="margin-bottom:0;">Death Valley Villa</h4>
				Price: $4.99
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Death Valley Villa/cover.jpg" style="width:100px; height:auto; display:block; margin:.5em auto;">
				<a href="/checkout?s2p-option=death-valley-villa" class="button small" style="margin-bottom:1em;">Proceed to Checkout</a>
			</div>
			<div class="column cta small">
				<h3 style="margin-top:0;">Bulk Savings</h3>
				<p>Get 4 games for only $10.00!</p>
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Crime and Prejudice/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Deadly Reunion/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Death Valley Villa/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" style="width:100px;">
				<div class="clear"></div>
				<a href="/bulk-savings" class="button small">Shop &amp; Save</a>
			</div>
		</div>
	</div><!-- pop -->
				
			</div><!-- .entry-content -->
			<div class="clear"></div>
	</div>
</div></article><!-- #post -->
			<h2>Reviews</h2>				
<div id="comments" class="comments-area">

			<h2 class="comments-title">
			3 thoughts on “<span>Death Valley Villa</span>”		</h2>

		<ol class="comment-list">
					<li id="comment-11" class="comment even thread-even depth-1">
			<article id="div-comment-11" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/9e254da4ef3747b415ffc84cea289963?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">Glenda L</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/death-valley-villa/#comment-11">
							<time datetime="2013-11-27T19:17:12+00:00">
								November 27, 2013 at 7:17 pm							</time>
						</a>
											</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Tons of fun if you’re into westerns. The characters are memorable, and the mystery was a challenge to solve. Definitely recommend.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/death-valley-villa/?replytocom=11#respond" onclick="return addComment.moveForm(&quot;div-comment-11&quot;, &quot;11&quot;, &quot;respond&quot;, &quot;36&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-16" class="comment odd alt thread-odd thread-alt depth-1">
			<article id="div-comment-16" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/98a399ab0f7ce13666fab1dda089bce6?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">faerygirl55</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/death-valley-villa/#comment-16">
							<time datetime="2013-11-27T21:16:36+00:00">
								November 27, 2013 at 9:16 pm							</time>
						</a>
											</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>This was a fun and simple game to host. Costumes and party decor were easy to find. The characters were distinctly different and I think everyone had a fun time acting them out. The story was intriguing. Really fun western themed murder mystery.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/death-valley-villa/?replytocom=16#respond" onclick="return addComment.moveForm(&quot;div-comment-16&quot;, &quot;16&quot;, &quot;respond&quot;, &quot;36&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-17" class="comment even thread-even depth-1">
			<article id="div-comment-17" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://0.gravatar.com/avatar/ad658d4b72474edaee22950570fba940?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">Randimus</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/death-valley-villa/#comment-17">
							<time datetime="2013-12-01T05:59:18+00:00">
								December 1, 2013 at 5:59 am							</time>
						</a>
											</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Well written, fun characters, not too many rules and a challenging plot. Vincent Price would approve.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/death-valley-villa/?replytocom=17#respond" onclick="return addComment.moveForm(&quot;div-comment-17&quot;, &quot;17&quot;, &quot;respond&quot;, &quot;36&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		</ol><!-- .comment-list -->

		
		
	
									<div id="respond" class="comment-respond">
				<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/store/death-valley-villa/#respond" style="display:none;">Cancel reply</a></small></h3>
									<form action="http://deadlyparties.com/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
																			<p class="comment-notes">Your email address will not be published. Required fields are marked <span class="required">*</span></p>							<p class="comment-form-author"><label for="author">Name <span class="required">*</span></label> <input id="author" name="author" type="text" value="John Vacca" size="30" aria-required="true"></p>
<p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input id="email" name="email" type="email" value="JohnJVacca@armyspy.com" size="30" aria-required="true"></p>
<p class="comment-form-url"><label for="url">Website</label> <input id="url" name="url" type="url" value="" size="30"></p>
												<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>						<p class="form-allowed-tags">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </code></p>						<p class="form-submit">
							<input name="submit" type="submit" id="submit" value="Post Comment">
							<input type="hidden" name="comment_post_ID" value="36" id="comment_post_ID">
<input type="hidden" name="comment_parent" id="comment_parent" value="0">
						</p>
						<p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="61804d7dad"></p>					</form>
							</div><!-- #respond -->
			
</div><!-- #comments -->								</div>