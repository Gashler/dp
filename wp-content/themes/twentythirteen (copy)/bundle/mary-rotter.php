<div id="content" class="site-content" role="main">

						
				
<article id="post-13" class="post-13 post type-post status-publish format-standard hentry category-store">
	<div class="bg"></div>
	<div class="content">
	<header class="entry-header">

				<h1 class="entry-title">
			Mary Rotter and the Half-bloody Fingerprints			<small>Murder Mystery Party Game</small>
					</h1>
				
		<div class="entry-meta">
			<!--<span class="date"><a href="http://deadlyparties.com/store/mary-rotter-and-the-half-bloody-fingerprints/" title="Permalink to Mary Rotter" rel="bookmark"><time class="entry-date" datetime="2013-10-12T20:28:58+00:00">October 12, 2013</time></a></span><span class="categories-links"><a href="http://deadlyparties.com/category/store/" title="View all posts in Store" rel="category tag">Store</a></span><span class="author vcard"><a class="url fn n" href="http://deadlyparties.com/author/admin/" title="View all posts by admin" rel="author">admin</a></span>			<span class="edit-link"><a class="post-edit-link" href="http://deadlyparties.com/wp-admin/post.php?post=13&amp;action=edit">Edit</a></span>-->
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

		<div class="entry-content">
		
		<div style="display: none">UA:D [1.9.22_1171]</div>
<div class="ratingblock ">
<div class="ratingheader "></div>
<div class="ratingstars ">
<div id="article_rater_13" class="ratepost gdsr-oxygen gdsr-size-20">
<div class="starsbar gdsr-size-20">
<div class="gdouter gdheight">
<div id="gdr_vote_a13" style="width: 100px;" class="gdinner gdheight"></div>
</div>
</div>
</div>
</div>
<div class="ratingtext ">
<div id="gdr_text_a13" class="voted inactive">Rating: 5.0/<strong>5</strong> (2 votes cast)</div>
</div>
</div>
<p><span class="hreview-aggregate"><span class="item"><span class="fn">Mary Rotter</span></span>, <span class="rating"><span class="average">5.0</span> out of <span class="best">5</span> based on <span class="votes">2</span> ratings <span class="summary"></span></span></span></p>
						<div class="column" style="position:relative;">
			<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" alt="Mary Rotter and the Half-bloody Fingerprints">
							<div class="discount">75%<br>Off</div>
					</div><!-- column -->
		<div class="column">
			<p style="margin-top:0;">In the heart of the wizarding world, within the enchanted walls of Pigzits, school of sorcery and soothsaying, someone has committed a crime of black magic, endangering the lives of every student and professor.</p>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Players:</th>
					<td>
						8					</td>
				</tr>
				<tr>
					<th>Gameplay:</th>
					<td>3 - 4 Hours</td>						
				</tr>
				<tr>
					<th>Ages:</th>
					<td>13 ↑</td>						
				</tr>
				<tr>
					<th>Rating:</th>
					<td>PG</td>						
				</tr>
				<tr>
					<th>Pages:</th>
					<td>≈ 65</td>						
				</tr>
			</tbody></table>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Formats:</th>
					<td>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf-50.png" alt="PDF" style="max-width:50px;">
						<div class="info" onclick="pop('pdf')">?</div>
						<div class="pop" id="pdf">
							<h3 style="margin-top:0;">Adobe PDF Format</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf.png" alt="PDF" style="max-width:140px;" class="alignleft">
							<p>If you prefer holding physical instructions, use the PDF format to make prints. The document also includes invitation templates, name tags, clues, and other resources that can be printed.</p>
						</div>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app-50.png" alt="web app" style="max-width:50px;">
						<div class="info" onclick="pop('web-app')">?</div>
						<div class="pop" id="web-app">
							<h3 style="margin-top:0;">Web App</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app.png" alt="web app" style="max-width:140px;" class="alignleft">
							<p>Use the web app for extra convenience and to cut down on printing costs. Each of your guests can access the game through any mobile device, laptop, or desktop.</p>
						</div>
					</td>
				</tr>
									<tr>
						<th>Retail:</th>
						<td>
							<div class="strike">$40.00<div class="line"></div></div>
						</td>
					</tr>
					<tr>
						<th>Your Price:</th>
						<td>
				    		$2.49<br>
							<small>($0.31/player)</small>
						</td>
					</tr>
							</tbody></table>
			<div class="clear"></div>
							<button onclick="pop('purchase-type')">Buy Now</button>
						<a href="/bulk-savings" class="button">Bulk Savings</a>
		</div><!-- column -->
		<div class="clear"></div>
		<br>
		

<ul class="tabs-menu">
	<li class="active gray" data-tab="details">Details</li>
	<li class="gray" data-tab="what-you-get">What You Get</li>
	<li class="gray" data-tab="backstory">Backstory</li>
	<li class="gray" data-tab="characters">Characters</li>
</ul>
<div class="clear"></div>
<div class="tabs-content">
	<div class="active block" data-tab="details">
		<h2>Details</h2>
		<ul>
			<li>Formats: Digital PDF / Web App</li>
			<li>Players:
				8 (4 males, 
				4 females)	
			</li>
			<li>Rating: PG</li>
			<li>Recommended Ages: 13 and up</li>
			<li>Approximate Pages: 65 single-sided / 33 double-sided</li>
			<li>Approximate Gameplay: 3 - 4 Hours</li>
			<li>Setting: The wizarding world (Urban Fantasy)</li>
			<li>Author(s): Stephen and Teresa Gashler</li>
		</ul>
	</div><!-- details -->
	<div class="block" data-tab="what-you-get">
		<h2>What You Get</h2>
		<ul>
			<li>Instructions for host</li>
			<li>Game rules</li>
			<li>Invitations to RSVP</li>
			<li>Character Assignment Invitations</li>
			<li>Name tag templates</li>
			<li>Character descriptions</li>
			<li>3 rounds of story for each character</li>
			<li>3 rounds of instructions for each character</li>
			<li>Tables for crime-solving notes</li>
		</ul>
	</div><!-- what-you-get -->
	<div class="block" data-tab="backstory">
		<h2>Backstory</h2>
		<p>It was a dark and foggy night around the ancient, stony walls of Pigzits school of magic. Though the spring semester had almost come to a close, the students and teachers were anything but giddy. The smell of trouble was in the air. On the evening following the disasters, in the great meeting hall, the wise, old headmaster, Professor Bunglebore, stood before the assembly of teachers and students. His kooky glasses reflected the flickers of the torches on the walls.</p><p>"Good evening wizards and witches," he said in his crackly voice. "I apologize for the sudden gathering, but I'm afraid we have business of the most urgent nature. As most of you know, I've been gone on important business for the last few days, and as to be expected, while I was subjecting you to harm by withdrawing my omnipotent presence (for your own good), the school was bombarded by the forces of evil. Certainly each of you is aware of the attack this morning. Though the halls were filled with chaos, I applaud our brave faculty for eventually driving out the minions of the dark lord and defeating the monster before any students were harmed.</p><p>"It's unfortunate that such a crisis should befall at the very time we lose our teacher of Battling Black Magic, Professor Fleamus Droopin, who has had important business of his own. I likewise regret placing such a heavy burden on our new teacher of Battling Black Magic, the famous Killroy Crockhart.</p>"However, this is not the first time we've had such an attack, though we hope it will be the last. What you must know is that it is not possible for the strong magic that protects the school to be broken from the outside. Thus, we're certain that someone on the inside has been opening the way for the minions of the dark lord to enter.<p></p><p>"It will surely be only a matter of time before our enemies return, putting the lives of every last one of us in danger. Therefore we must discover the identity of this traitor -- who is undoubtedly within the sound of my voice -- before it's too late. Until this mystery is solved, no one is allowed to leave this hall."	</p></div>
	<div class="block" data-tab="characters">
		<h2>Characters</h2>
						<h3 style="margin-bottom:0;" id="Mary Rotter">Mary Rotter</h3>
				<p style="margin:0;">(Student)</p>
				<p style="margin:.5em 0;"><em>You're the most famous student in Pigzits, known for your frequent run-ins with the dark lord and your knack for overthrowing his evil designs at the last moment. You're pretty cool. Your best friends are Donald Beezely and Crimany Stranger.</em></p>
						<h3 style="margin-bottom:0;" id="Crimany Stranger">Crimany Stranger</h3>
				<p style="margin:0;">(Student)</p>
				<p style="margin:.5em 0;"><em>You're by far the best (not to mention the most intelligent, gifted, smart, and not-dumb) student Pigzits has ever had. Your best friends are Mary Rotter and Donald Beezely. Without you as the brains of the operations, your little team of teenage crime-solving sleuths would never have outwitted the dark lord over and over ... and over again.</em></p>
						<h3 style="margin-bottom:0;" id="Donald Beezely">Donald Beezely</h3>
				<p style="margin:0;">(Student)</p>
				<p style="margin:.5em 0;"><em>You're kind of like your best friends Mary Rotter and Crimany Stranger only not as brave or intelligent.</em></p>
						<h3 style="margin-bottom:0;" id="Killroy Crockhart">Killroy Crockhart</h3>
				<p style="margin:0;">(New teacher of Battling Black Magic)</p>
				<p style="margin:.5em 0;"><em>As a best-selling author, wizarding superstar, and heartthrob among the ladies, you're everything you've ever dreamed of and more.</em></p>
						<h3 style="margin-bottom:0;" id="Fleamus Droopin">Fleamus Droopin</h3>
				<p style="margin:0;">(Former teacher of Battling Black Magic)</p>
				<p style="margin:.5em 0;"><em>You're kind of cool in that you actually know how to battle evil through your magical prowess. As far as the rest of you goes, your middle-aged, down-and-out, and frankly rather boring. That is, except for your deep, dark secret. That totally makes up for your lack of coolness.</em></p>
						<h3 style="margin-bottom:0;" id="Dribble Screwlany">Dribble Screwlany</h3>
				<p style="margin:0;">(Teacher of Fortune Telling)</p>
				<p style="margin:.5em 0;"><em>When old hippies become teachers ...</em></p>
						<h3 style="margin-bottom:0;" id="Dishevelrous Scrape">Dishevelrous Scrape</h3>
				<p style="margin:0;">(Teacher of Black Magic)</p>
				<p style="margin:.5em 0;"><em>You're dark, scary, and evil. You hate children and have few redeemable qualities. Thankfully, this is the wizarding world, where it's okay to be eccentric.</em></p>
						<h3 style="margin-bottom:0;" id="Ruby Shmagrid">Ruby Shmagrid</h3>
				<p style="margin:0;">(Groundskeeper and caretaker of insanely-dangerous magical creatures)</p>
				<p style="margin:.5em 0;"><em>You're unbelievably big and brutish yet kind to the smallest field mouse.</em></p>
			</div>
	<div class="pop" id="purchase-type">
		<div class="columns center">
			<div class="column small">
				<h3>Ala Carte</h3>
				<h4 style="margin-bottom:0;">Mary Rotter and the Half-bloody Fingerprints</h4>
				Price: $4.99
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" style="width:100px; height:auto; display:block; margin:.5em auto;">
				<a href="/checkout?s2p-option=mary-rotter-and-the-half-bloody-fingerprints" class="button small" style="margin-bottom:1em;">Proceed to Checkout</a>
			</div>
			<div class="column cta small">
				<h3 style="margin-top:0;">Bulk Savings</h3>
				<p>Get 4 games for only $10!</p>
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Crime and Prejudice/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Deadly Reunion/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Death Valley Villa/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" style="width:100px;">
				<div class="clear"></div>
				<a href="/bulk-savings" class="button small">Shop &amp; Save</a>
			</div>
		</div>
	</div><!-- pop -->
				
			</div><!-- .entry-content -->
			<div class="clear"></div>
	</div>
</div></article><!-- #post -->
			<h2>Reviews</h2>				
<div id="comments" class="comments-area">

			<h2 class="comments-title">
			2 thoughts on “<span>Mary Rotter</span>”		</h2>

		<ol class="comment-list">
					<li id="comment-12" class="comment even thread-even depth-1">
			<article id="div-comment-12" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/31f0a92edaec50d7e47131d9ed082ba2?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">John Vacca</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/mary-rotter-and-the-half-bloody-fingerprints/#comment-12">
							<time datetime="2013-11-27T19:21:34+00:00">
								November 27, 2013 at 7:21 pm							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=12">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Where to start … I loved the fact that this game’s obviously a Harry Potter spoof, and yet the names are all different. The satire was hilarious. This is one you’ll definitely want to put on costumes for and play a Harry Potter soundtrack. So much fun.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/mary-rotter-and-the-half-bloody-fingerprints/?replytocom=12#respond" onclick="return addComment.moveForm(&quot;div-comment-12&quot;, &quot;12&quot;, &quot;respond&quot;, &quot;13&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-14" class="comment odd alt thread-odd thread-alt depth-1">
			<article id="div-comment-14" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/98a399ab0f7ce13666fab1dda089bce6?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">faerygirl55</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/mary-rotter-and-the-half-bloody-fingerprints/#comment-14">
							<time datetime="2013-11-27T19:48:54+00:00">
								November 27, 2013 at 7:48 pm							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=14">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Ok, I was weirded out at first by the gender changes. They made Harry into Mary and Rubeus Hagrid into Ruby Shmagrid?! Wasn’t sure if I was a fan of that, but I did buy the 4 games deal, so I thought why not give it a try?</p>
<p>I quickly learned that this game is extremely satirical. If you’re a Harry Potter diehard, just be warned that this makes fun of it, but in a very tasteful way. Similar to the Crime and Prejudice game, this game takes liberties and is not true to the books at all (it puts Lockhart and Lupin in the same story, for one). </p>
<p>Once I got past those things, it turned out to be a fun game. And the gender changes? Not really a big deal. If someone was really bothered by it, they could just change it back. </p>
<p>I thought the murder was cleverly written. Totally threw us off guard. I’m tempted to host this one again, even though I already know how it ends.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/mary-rotter-and-the-half-bloody-fingerprints/?replytocom=14#respond" onclick="return addComment.moveForm(&quot;div-comment-14&quot;, &quot;14&quot;, &quot;respond&quot;, &quot;13&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		</ol><!-- .comment-list -->

		
		
	
									<div id="respond" class="comment-respond">
				<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/store/mary-rotter-and-the-half-bloody-fingerprints/#respond" style="display:none;">Cancel reply</a></small></h3>
									<form action="http://deadlyparties.com/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
																			<p class="logged-in-as">Logged in as <a href="http://deadlyparties.com/wp-admin/profile.php">admin</a>. <a href="http://deadlyparties.com/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fdeadlyparties.com%2Fstore%2Fmary-rotter-and-the-half-bloody-fingerprints%2F&amp;_wpnonce=6bd26a3463" title="Log out of this account">Log out?</a></p>																			<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>						<p class="form-allowed-tags">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </code></p>						<p class="form-submit">
							<input name="submit" type="submit" id="submit" value="Post Comment">
							<input type="hidden" name="comment_post_ID" value="13" id="comment_post_ID">
<input type="hidden" name="comment_parent" id="comment_parent" value="0">
						</p>
						<input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment" value="bcd5b77b40"><script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
<p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="284712c78a"></p>					</form>
							</div><!-- #respond -->
			
</div><!-- #comments -->								</div>