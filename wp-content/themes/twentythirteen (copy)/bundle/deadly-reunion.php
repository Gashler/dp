<div id="content" class="site-content" role="main">

						
				
<article id="post-80" class="post-80 post type-post status-publish format-standard hentry category-store">
	<div class="bg"></div>
	<div class="content">
	<header class="entry-header">

				<h1 class="entry-title">
			Deadly Reunion			<small>Murder Mystery Party Game</small>
					</h1>
				
		<div class="entry-meta">
			<!--<span class="date"><a href="http://deadlyparties.com/store/deadly-reunion/" title="Permalink to Deadly Reunion" rel="bookmark"><time class="entry-date" datetime="2013-11-02T04:10:01+00:00">November 2, 2013</time></a></span><span class="categories-links"><a href="http://deadlyparties.com/category/store/" title="View all posts in Store" rel="category tag">Store</a></span><span class="author vcard"><a class="url fn n" href="http://deadlyparties.com/author/admin/" title="View all posts by admin" rel="author">admin</a></span>			<span class="edit-link"><a class="post-edit-link" href="http://deadlyparties.com/wp-admin/post.php?post=80&amp;action=edit">Edit</a></span>-->
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

		<div class="entry-content">
		
		<div style="display: none">UA:D [1.9.22_1171]</div>
<div class="ratingblock ">
<div class="ratingheader "></div>
<div class="ratingstars ">
<div id="article_rater_80" class="ratepost gdsr-oxygen gdsr-size-20">
<div class="starsbar gdsr-size-20">
<div class="gdouter gdheight">
<div id="gdr_vote_a80" style="width: 100px;" class="gdinner gdheight"></div>
</div>
</div>
</div>
</div>
<div class="ratingtext ">
<div id="gdr_text_a80" class="voted inactive">Rating: 5.0/<strong>5</strong> (3 votes cast)</div>
</div>
</div>
<p><span class="hreview-aggregate"><span class="item"><span class="fn">Deadly Reunion</span></span>, <span class="rating"><span class="average">5.0</span> out of <span class="best">5</span> based on <span class="votes">3</span> ratings <span class="summary"></span></span></span></p>
			<div class="column" style="position:relative;">
			<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Deadly Reunion/cover.jpg" alt="Deadly Reunion">
			<div class="discount">75%<br>Off</div>
			</div><!-- column -->
		<div class="column">
			<p style="margin-top:0;">Before Uncle Bob, an eccentric multi-millionaire, dies of a terminal illness, he invites his extended family to his private island. Whoever impresses him most will become the sole heir of his mansion. But after three hellish days of feuding cousins, vehicles accidents, attacks from wild animals, and a mysterious murder, the motley family is forced to work together before it's too late.</p>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Players:</th>
					<td>
						8					</td>
				</tr>
				<tr>
					<th>Gameplay:</th>
					<td>3 - 4 Hours</td>						
				</tr>
				<tr>
					<th>Ages:</th>
					<td>13 ↑</td>						
				</tr>
				<tr>
					<th>Rating:</th>
					<td>PG</td>						
				</tr>
				<tr>
					<th>Pages:</th>
					<td>≈ 65</td>						
				</tr>
			</tbody></table>
			<table class="gray mini-table">
				<tbody><tr>
					<th>Formats:</th>
					<td>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf-50.png" alt="PDF" style="max-width:50px;">
						<div class="info" onclick="pop('pdf')">?</div>
						<div class="pop" id="pdf">
							<h3 style="margin-top:0;">Adobe PDF Format</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/pdf.png" alt="PDF" style="max-width:140px;" class="alignleft">
							<p>If you prefer holding physical instructions, use the PDF format to make prints. The document also includes invitation templates, name tags, clues, and other resources that can be printed.</p>
						</div>
						<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app-50.png" alt="web app" style="max-width:50px;">
						<div class="info" onclick="pop('web-app')">?</div>
						<div class="pop" id="web-app">
							<h3 style="margin-top:0;">Web App</h3>
							<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/web-app.png" alt="web app" style="max-width:140px;" class="alignleft">
							<p>Use the web app for extra convenience and to cut down on printing costs. Each of your guests can access the game through any mobile device, laptop, or desktop.</p>
						</div>
					</td>
				</tr>
									<tr>
						<th>Retail:</th>
						<td>
							<div class="strike">$40.00<div class="line"></div></div>
						</td>
					</tr>
					<tr>
						<th>Your Price:</th>
						<td>
				    $2.49<br>
							<small>($0.31/player)</small>
						</td>
					</tr>
							</tbody></table>
			<div class="clear"></div>
							<button onclick="pop('purchase-type')">Buy Now</button>
						<a href="/bulk-savings" class="button">Bulk Savings</a>
		</div><!-- column -->
		<div class="clear"></div>
		<br>
		

<ul class="tabs-menu">
	<li class="active gray" data-tab="details">Details</li>
	<li class="gray" data-tab="what-you-get">What You Get</li>
	<li class="gray" data-tab="backstory">Backstory</li>
	<li class="gray" data-tab="characters">Characters</li>
</ul>
<div class="clear"></div>
<div class="tabs-content">
	<div class="active block" data-tab="details">
		<h2>Details</h2>
		<ul>
			<li>Formats: Digital PDF / Web App</li>
			<li>Players:
				8				(4				males, 
				4 females)	
			</li>
			<li>Rating: PG</li>
			<li>Recommended Ages: 13 and up</li>
			<li>Approximate Pages: 65 single-sided / 33 double-sided</li>
			<li>Approximate Gameplay: 3 - 4 Hours</li>
			<li>Setting: A family reunion on a private island</li>
			<li>Author(s): Stephen Gashler</li>
		</ul>
	</div><!-- details -->
	<div class="block" data-tab="what-you-get">
		<h2>What You Get</h2>
		<ul>
			<li>Instructions for host</li>
			<li>Game rules</li>
			<li>Invitations to RSVP</li>
			<li>Character Assignment Invitations</li>
			<li>Name tag templates</li>
			<li>Character descriptions</li>
			<li>3 rounds of story for each character</li>
			<li>3 rounds of instructions for each character</li>
			<li>Tables for crime-solving notes</li>
		</ul>
	</div><!-- what-you-get -->
	<div class="block" data-tab="backstory">
		<h2>Backstory</h2>
		<p>Though nearly every member of the O'donnel, Silverstein, Prismall, Drummond, and McCrystal families received the invitation from their Great Uncle Bob, only a few knew they even had an Uncle Bob. The rich, old eccentric had lived a secret life on a private island. Now he was suffering from a terminal illness, and before he kicked the bucket, he wanted to reconcile himself to his estranged family, from whom he would choose a single heir to inherit his mansion. All he asked was that those interested would visit him for a three day family reunion.</p><p>On Thursday afternoon, at the appointed time, only five people showed up to the dock: Captain Darryl Drummond -- the celebrated hero of the air force -- his talkative wife Lydia, Jimmy Silverstein -- a young TV talk show host with big dreams -- Victoria Prismall -- an introverted artist with very black wardrobe -- and Tony O'donnel -- your typical Italian macho man, complete with wife beater, tattoos, and gold necklace. After a bit of smalltalk, it was clear that these extended family members were as unfamiliar with each other as they were with Uncle Bob.</p><p>They were met at the dock by a ferryman who turned out to be Uncle Bob himself, telling them to step into his speedboat. With his bald head and white mustache, he looked just like the iconic millionaire from a board game. His gold teeth and slightly wicked laugh had a way of making his guests uneasy.</p><p>The tiny island was beautiful and lush, and Uncle Bob's mansion was no less than spectacular. But just as everyone was about to step inside, another luxurious speed boat approached the shore. Its occupants turned out to be more relatives whom no one knew: James McCrystal, a well-to-do plastic surgeon, and his lovely wife, Zora. Perhaps there was something a little <em>too</em> lovely about her tight skin and thick eyeshadow.</p><p>Within the mansion, a candlelit dinner awaited the guests, as prepared by Uncle Bob's housekeeper, Mercy Shields. Though she attended to their every need and want with her impeccable hospitality, there was something stern in her eyes.</p><p>For the following two days, the family members put on smiles for meals, games, and "getting to know you" activities as led by Uncle Bob, and it almost seemed uncanny how many things went wrong, from vehicle wrecks, to hunting injuries, to house fires. Following a disastrous family talent show, the time finally came for the long-awaited "closing speech" in which Uncle Bob would announce his chosen heir. Finally there was an end in sight, and nothing else could possibly go wrong. As the family sat around the parlor room in anticipation, the old man downed his glass of strawberry lemonade, then stood up and said:</p><p>"My dear family, whom I have grown so close to over these last few days ... while choosing an heir for my mansion has not been an easy task, I've finally come to a decision. The one who will inherit my exorbitant droves of wealth, becoming filthy rich and destined for a life of pampered eccentricity, is none other than --" But before he could finish his sentence, the ceiling collapsed, showering everyone with debris. Wood crashed against the floor, and suddenly there were bees everywhere. Screaming (and buzzing) pandemonium ensued. When, after opening every window and door, the majority of the bees had flown away, Uncle Bob was found on the floor ... dead. Though a few bee stings and wood splinters were found on his body, the cause of his death wasn't clear.</p><p>With no access to the island but by boat, it would be hours, if not days, before the police would show up. As for those already on the island, the two boats they had were completely out of gas. As every one of them was gathered in the same room, they began to look at each other with suspicion. "He could have had a heart attack," someone said. "Maybe it was the terminal illness," said someone else. "Surely it was the bees," reasoned another. But after the last three days, most everyone was of a different opinion: there was a murderer in their midst.</p>	</div>
	<div class="block" data-tab="characters">
		<h2>Characters</h2>
						<h3 style="margin-bottom:0;" id="James McCrystal">James McCrystal</h3>
				<p style="margin:0;">(Dermatologist)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>You're a wealthy plastic surgeon who enjoys tender steaks and exotic golf courses. You've made it your business in life to always have the best of everything, from your speed boat to your trophy wife (whether or not she's a pain to live with). After all, you've worked to hard to get where you are in life, and you deserve the best.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Zora McCrystal">Zora McCrystal</h3>
				<p style="margin:0;">(Wife of James McCrystal)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>You've never worked a day in your life. There was no need to, what with all boys who were mesmerized in your young beauty. Now that you're getting up there in years, it's wonderful to be married to a plastic surgeon, who can ensure that age doesn't diminish your power over the opposite sex. In the rare event that you can't get what you want through your alluring charm, you've been known to be a little ... mean.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Darryl Drummond">Darryl Drummond</h3>
				<p style="margin:0;">(Air force captain)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>Though a man of order and precision, to those who know you best, you're as cuddly as a teddy bear. If you had a few million dollars to throw around, you would build orphanages, arboretums, and petting zoos.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Lydia Drummond">Lydia Drummond</h3>
				<p style="margin:0;">(Barber)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>You love to talk. And why shouldn't you? Being married to a very important captain in the air force, you've seen the world. True, you're just a barber, though you have big plans of becoming a bestselling author someday ... as soon as you learn how to write.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Jimmy Silverstein">Jimmy Silverstein</h3>
				<p style="margin:0;">(TV host)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>You've always been a visionary, bringing compelling stories to light through motion pictures. At least that's the dream. Though you're currently working as a TV producer for a small show on a lesser-known channel, you know it's only a matter of time before your storytelling skills go big.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Victoria Prismall">Victoria Prismall</h3>
				<p style="margin:0;">(Artist)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>You're not a fan of modernity. Or politics. Or people. Perhaps it's because no one understands you, not to mention your art. But then, how many people see reality for what it really is? Crooked shapes, rough lines, dark colors ... the bitter truth that no one but yourself is willing to face. When people tell you it looks like the scribbling of an ape, you tell them to shut up.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Tony O" donnel'="">Tony O'donnel</h3>
				<p style="margin:0;">(Mechanic)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>You were a bit of a bully in high school. While the nerds you pestered have gone on to start software companies and drive BMW's, you have to climb under their cars and soil your hands with axle grease. At least you have the satisfaction of remembering the time when you'd stuffed these morons into garbage cans. Those were the glory days. When life starts to depress you, thank goodness there's beer.</em></p><p></p>
						<h3 style="margin-bottom:0;" id="Mercy Shields">Mercy Shields</h3>
				<p style="margin:0;">(Housekeeper)</p>
				<p style="margin:.5em 0;"><em></em></p><p><em>Having worked over two decades for an eccentric, old, millionaire whose business was sometimes a little less than legitimate, you've learned when to keep your mouth shut and your ears open. The fact that, over the years, every staff member but you has been fired, is proof that you're good at what you do. Whenever the boss throws one of his tantrums, you politely remind him of the blackmail you hold against him, and everything works out nicely. You're not about to pack up, because if anyone deserves this house, it's you.</em></p><p></p>
			</div>
	<div class="pop" id="purchase-type">
		<div class="columns center">
			<div class="column small">
				<h3>Ala Carte</h3>
				<h4 style="margin-bottom:0;">Deadly Reunion</h4>
				Price: $4.99
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Deadly Reunion/cover.jpg" style="width:100px; height:auto; display:block; margin:.5em auto;">
				<a href="/checkout?s2p-option=deadly-reunion" class="button small" style="margin-bottom:1em;">Proceed to Checkout</a>
			</div>
			<div class="column cta small">
				<h3 style="margin-top:0;">Bulk Savings</h3>
				<p>Get 4 games for only $10!</p>
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Crime and Prejudice/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Deadly Reunion/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Death Valley Villa/cover.jpg" style="width:100px;">
				<img src="http://deadlyparties.com/wp-content/themes/twentythirteen/images/games/Mary Rotter and the Half-bloody Fingerprints/cover.jpg" style="width:100px;">
				<div class="clear"></div>
				<a href="/bulk-savings" class="button small">Shop &amp; Save</a>
			</div>
		</div>
	</div><!-- pop -->
				
			</div><!-- .entry-content -->
			<div class="clear"></div>
	</div>
</div></article><!-- #post -->
			<h2>Reviews</h2>				
<div id="comments" class="comments-area">

			<h2 class="comments-title">
			3 thoughts on “<span>Deadly Reunion</span>”		</h2>

		<ol class="comment-list">
					<li id="comment-3" class="comment even thread-even depth-1">
			<article id="div-comment-3" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/dd8ccea6bdfafc4aa094c9280e352e75?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">Richard Dominguez</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/deadly-reunion/#comment-3">
							<time datetime="2013-11-23T23:50:15+00:00">
								November 23, 2013 at 11:50 pm							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=3">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>This game was a blast. The writing is fantastic, the characters are fun, and the plot is hilarious. I was always wondering what was coming next. Definitely a challenge to solve, though. Not for kids.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/deadly-reunion/?replytocom=3#respond" onclick="return addComment.moveForm(&quot;div-comment-3&quot;, &quot;3&quot;, &quot;respond&quot;, &quot;80&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-7" class="comment odd alt thread-odd thread-alt depth-1">
			<article id="div-comment-7" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/bd4c9a4b92be466089ec5ac093ba4082?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">Jennifer Blake</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/deadly-reunion/#comment-7">
							<time datetime="2013-11-24T04:31:14+00:00">
								November 24, 2013 at 4:31 am							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=7">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>I enjoyed playing this game.  We had a good group of people who (some had played another game on this site)knew how to share the information needed but still keep secrets based on our game instructions.  It was a great story. Well-written.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/deadly-reunion/?replytocom=7#respond" onclick="return addComment.moveForm(&quot;div-comment-7&quot;, &quot;7&quot;, &quot;respond&quot;, &quot;80&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		<li id="comment-15" class="comment even thread-even depth-1">
			<article id="div-comment-15" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<img alt="" src="http://1.gravatar.com/avatar/98a399ab0f7ce13666fab1dda089bce6?s=74&amp;d=monsterid&amp;r=G" class="avatar avatar-74 photo" height="74" width="74">						<b class="fn">faerygirl55</b> <span class="says">says:</span>					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="http://deadlyparties.com/store/deadly-reunion/#comment-15">
							<time datetime="2013-11-27T20:32:32+00:00">
								November 27, 2013 at 8:32 pm							</time>
						</a>
						<span class="edit-link"><a class="comment-edit-link" href="http://deadlyparties.com/wp-admin/comment.php?action=editcomment&amp;c=15">Edit</a></span>					</div><!-- .comment-metadata -->

									</footer><!-- .comment-meta -->

				<div class="comment-content">
					<p>Make sure all your guests understand the rules of the game before you start! This was the third murder mystery I hosted, and some of my guests had played in my earlier games while others had not. It says in the instructions that you have to decide for your character what things to keep secret because you might be the murderer. Someone in our game didn’t understand that and thought they were just supposed to reveal everything… whoops! I made it clear after the first round, but it did give some things away.</p>
<p>Despite that, still a fun game. I have great friends who are willing to get into any character, and there are some… interesting ones in this game. The story was funny.</p>
				</div><!-- .comment-content -->

				<div class="reply">
					<a class="comment-reply-link" href="/store/deadly-reunion/?replytocom=15#respond" onclick="return addComment.moveForm(&quot;div-comment-15&quot;, &quot;15&quot;, &quot;respond&quot;, &quot;80&quot;)">Reply</a>				</div><!-- .reply -->
			</article><!-- .comment-body -->
</li><!-- #comment-## -->
		</ol><!-- .comment-list -->

		
		
	
									<div id="respond" class="comment-respond">
				<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="/store/deadly-reunion/#respond" style="display:none;">Cancel reply</a></small></h3>
									<form action="http://deadlyparties.com/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
																			<p class="logged-in-as">Logged in as <a href="http://deadlyparties.com/wp-admin/profile.php">admin</a>. <a href="http://deadlyparties.com/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fdeadlyparties.com%2Fstore%2Fdeadly-reunion%2F&amp;_wpnonce=6bd26a3463" title="Log out of this account">Log out?</a></p>																			<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>						<p class="form-allowed-tags">You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;strike&gt; &lt;strong&gt; </code></p>						<p class="form-submit">
							<input name="submit" type="submit" id="submit" value="Post Comment">
							<input type="hidden" name="comment_post_ID" value="80" id="comment_post_ID">
<input type="hidden" name="comment_parent" id="comment_parent" value="0">
						</p>
						<input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment" value="f21b083963"><script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
<p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="b4eb369cde"></p>					</form>
							</div><!-- #respond -->
			
</div><!-- #comments -->								</div>