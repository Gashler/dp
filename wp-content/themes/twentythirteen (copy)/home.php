<?php get_header(); ?>
	<a href="/bulk-savings">
		<div id="home-hero-container">
			<div id="home-hero-offer-banner">Get 4 Games for Only $19.99</div>
			<img id="home-hero-offer" src="<?php echo get_template_directory_uri() ?>/images/home-hero-offer.png" alt="Get 4 Games for Only $19.99">
			<div id="home-hero-caption">
				<div class="bg dark"></div>
				<h2>
					<img src="<?php echo get_template_directory_uri() ?>/images/host-your-own-murder-mystery.png" alt="Host Your Own Murder Mystery">
				</h2>
				<a href="/bulk-savings" class="button">Bulk Savings</a>
			</div>
			<div id="home-hero-icons">
				<img src="<?php echo get_template_directory_uri() ?>/images/mediums.png" width="225" alt="Download PDF or play online web app">
			</div>
			<img id="home-hero" src="<?php echo get_template_directory_uri() ?>/images/home-hero-2.jpg">
		</div>
	</a>
	<div id="primary" class="content-area">
		<div class="bg"></div>
		<div id="content" class="site-content tiles" role="main">
			<?php query_posts('cat=2&showposts=20'); ?>
			<?php if ( have_posts() ) : ?>
	
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="bg"></div>
						<div class="content">
						<header class="entry-header">
							<h2 class="entry-title">
								<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h2>
							<div class="entry-content">
								<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
								<?php
									$game_db = get_field('game_db');
									$download_id = get_field('download_id');
									$con=mysqli_connect("localhost","root","Penguin4Shpenguin!?",$game_db);
									$result = mysqli_query($con,"SELECT * FROM game");
									while($row = mysqli_fetch_array($result)) {
										$title = $row['title'];
										$author = $row['author'];
										$description = $row['description'];
										$rating = $row['rating'];
										$pages = $row['pages'];
										$backStory = $row['back_story'];
									}
								?>
							<a href="<?php the_permalink(); ?>">
								<div style="position:relative; margin-top:-1em;">
									<?php
										$discount = get_field('discount');
										if ($discount !== "0") {
									?>
										<div class="discount"><?php the_field('discount') ?>%<br>Off</div>
									<?php } ?>
									<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover-small.jpg" alt="<?php echo $title ?>" class="alignleft">
								</div>
							</a>
							</div><!-- entry-content -->
							<div class="entry-meta">
								<!--<?php twentythirteen_entry_meta(); ?>
								<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>-->
							</div><!-- .entry-meta -->
						</header><!-- .entry-header -->
					
						<?php if ( is_search() ) : // Only display Excerpts for Search ?>
						<div class="entry-summary">
							<?php the_excerpt(); ?>
						</div><!-- .entry-summary -->
						<?php endif; ?>
						<div style="text-align:center">
							<a class="button center small" href="<?php the_permalink(); ?>" rel="bookmark">View Game</a>
						</div>
						<footer class="entry-meta">
							<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
								<?php get_template_part( 'author-bio' ); ?>
							<?php endif; ?>
						</footer><!-- .entry-meta -->
						<div class="clear"></div>
						</div>
					</article><!-- #post -->
				<?php endwhile; ?>
	
				<?php twentythirteen_paging_nav(); ?>
			<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>