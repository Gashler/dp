<?php
	$con=mysqli_connect("localhost","root","Penguin4Shpenguin!?",$game);
	$result = mysqli_query($con,"SELECT * FROM game");
	while($row = mysqli_fetch_array($result)) {
		$title = $row['title'];
		$description = $row['description'];
		$author = $row['author'];
		$backStory = $row['back_story'];
		$setting = $row['setting'];
		$epilogue = $row['epilogue'];
	}
	$result = mysqli_query($con,"SELECT * FROM players");
	$print = $_GET['print'];
	if ($print == null) $print = false;
?> 
<article>
	<div id="content">
	<div class="general">
	<div class="center" align="center">
		<h1><?php echo $title ?></h1>
		<h3>A Murder Mystery Game<br>By <?php echo $author ?></h3>
		<br>
		<?php if ($print == true) { ?>
			<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover.jpg" width="450" height="450" style="width:450px; height:450px;">
		<?php } else { ?>
			<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover.jpg" width="450" height="450" style="width:100%; height:auto; max-width:450px;">		
		<?php } ?>
		<p>
			<?php
				$result = mysqli_query($con,"SELECT count(1) FROM players");
				$row = mysqli_fetch_array($result);
				$total = $row[0];
				echo $total;
			?>
			Players<br>
			<?php
				$result = mysqli_query($con,"SELECT count(1) FROM players WHERE sex = 'male'");
				$row = mysqli_fetch_array($result);
				$male = $row[0];
				echo "(" . $male;
			?>
			men, 
			<?php
				$result = mysqli_query($con,"SELECT count(1) FROM players WHERE sex = 'female'");
				$row = mysqli_fetch_array($result);
				$female = $row[0];
				echo $female . " women)";
			?>
		</p>
		<?php if ($print == true) { ?>
			<img src="<?php echo get_template_directory_uri() ?>/images/logo-bw.png" style="width:297px;" width="297">
		<?php } else { ?>
			<img src="<?php echo get_template_directory_uri() ?>/images/logo-bw.png" style="width:100%; height:auto; max-width:297px;" width="297">
		<?php } ?>
		<!--<p>Copyright &copy; <?php echo date('Y') ?> DeadlyParties.com</p>-->
	</div>
	</div><!-- general -->
	<div class="host">
		<?php if ($print == false) { ?>
			<h1>Contents</h1>
			<h3>Setup</h3>
			<ul>
				<li><a href="#how-to-play">How to Play</a></li>
				<li><a href="#characters">Characters</a></li>
				<li><a href="#name-tags">Name Tags</a></li>
				<li><a href="#back-story">Back Story</a></li>
				<li><a href="#clues">Clues</a></li>
			</ul>
			<h3>Character Information Sheets</h3>
			<ul>
				<?php
					$result = mysqli_query($con,"SELECT * FROM players ORDER BY name ASC");
					while($row = mysqli_fetch_array($result)) {
						echo "<li><a href='#" . $row['name'] . "'>" . $row['name'] . " - " . $row['role'] . "</a></li>";
					}
				?>
			</ul>
			<?php if ($epilogue !== null) { ?>
				<h3>Epilogue</h3>
				<ul>
					<li><a href="#epilogue">Epilogue</a></li>
				</ul>
			<?php } ?>
		<?php } ?>
		<h1><?php echo $title ?></h1>
		<p>Setting: <?php echo $setting ?></p>
		<p><?php echo $description ?></p>
		<h1>How to Play</h1>
		<?php include "how-to-play.php" ?>
	<?php if ($print == true) { ?>
		<h1 id="characters">Characters</h1>
		<ol>
			<?php
				$result = mysqli_query($con,"SELECT * FROM players ORDER BY name ASC");
				while($row = mysqli_fetch_array($result)) {
					echo "<li><!--<a href='#" . $row['name'] . "'>-->" . $row['name'] . "<!--</a>-->";
					if ($row['role'] !== null) {
						echo " - " . $row['role'];
					}
					echo " (" . $row['sex'] . ")</li>";
				}
			?>
		</ol>
		<h2>Invitations</h2>
		<p>Print as many copies of the following invitation template as needed. Print only one copy of each of the character assignment invitations for the guests who RSVP.</p>
		<h6 style="height:1px; margin:0;">&nbsp;</h6>
		<table cellpadding="10" cellspacing="0" border="1" bordercolor="gray">
			<tr>
				<td colspan="3">
					<div align="center">
						<h2 style="margin:0;"><small style="font-family:Times; font-weight:100; font-size:12pt;">You're invited to attend ...</small><br><?php echo $title ?></h2>
						<img src="<?php echo get_template_directory_uri() ?>/images/logo-bw.png" style="width:200px;" width="200">
					</div>
				</td>
			</tr>
			<tr>
				<td width="300" rowspan="7">
					<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover-small.jpg" width="300" height="300">
					<p style="line-height:1em;"><small><strong>Please RSVP before __________________.</strong> &nbsp;After you RSVP, you'll receive additional information about the role you'll be playing. It's going to be a blast!</small></p>
				</td>
				<th align="right">Date:</th>
				<td width="40%"></td>
			</tr>
			<tr>
				<th align="right">Time:</th>
				<td></td>
			</tr>
			<tr>
				<th align="right" valign="top">Place:</th>
				<td valign="top"></td>
			</tr>
			<tr>
				<th align="right" valign="top">RSVP:</th>
				<td valign="top"></td>
			</tr>
			<tr>
				<th align="right" valign="top">Phone:</th>
				<td valign="top"></td>
			</tr>
			<tr>
				<th align="right" valign="top">Email:</th>
				<td valign="top"></td>
			</tr>
			<tr>
				<th align="right" valign="top">Other:</th>
				<td valign="top"></td>
			</tr>
		</table>
		<?php
			$result = mysqli_query($con,"SELECT * FROM players ORDER BY name ASC");
			$x = 1;
			while($row = mysqli_fetch_array($result)) { ?>
				<h6 style="height:1px; margin:0;">&nbsp;</h6>
				<table cellpadding="10" cellspacing="0" border="1" bordercolor="gray">
					<tr>
						<td colspan="2">
							<div align="center">
								<h2 style="margin:0;"><small style="font-family:Times; font-weight:100; font-size:12pt;">Thank you for your RSVP. You're scheduled to attend ...</small><br><?php echo $title ?></h2>
								<img src="<?php echo get_template_directory_uri() ?>/images/logo-bw.png" style="width:200px;" width="200">
							</div>
						</td>
					</tr>
					<tr>
						<td width="300" valign="top">
							<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title ?>/cover-small.jpg" width="300" height="300" style="width:300px; width:300px;">
						</td>
						<td align="left" valign="top" width="50%">
							<strong>Your Character:</strong> <?php echo $row['name'] ?><br>
							<strong>Description:</strong>&nbsp;
							<?php if ($row['role'] !== null) { ?>
								<?php echo "(" . $row['role'] . ")" ?>
							<?php
								}
								echo "<br><br>" . $row['description'];
							?>
							<p style="line-height:1em;">
								<small>Please come in costume and character, and please arrive on time. The game cannot be completed unless every character is present. It's going to be a blast!</small>
							</p>
							<strong>Notes:</strong>
						</td>
					</tr>
				</table>
			<?php } ?>
		<h1 id="name-badges">Name Tags</h1>
		<p>For best results, print the following name tags on card stalk. Provide them to your guests along with pins or tape (sometimes paperclips can also do the job).</p>
		<table cellpadding="30" cellspacing="0" border="1" width="100%" style="width:100%;">
			<?php
				$result = mysqli_query($con,"SELECT * FROM players ORDER BY name ASC");
				$x = 1;
				while($row = mysqli_fetch_array($result)) {
					// if odd number
					if ($x % 2 !== 0) echo "<tr>";
					echo "<td width='50%'><h5 style='margin:0;'>" . $row['name'] . "</h5>";
					// if even number
					if ($x % 2 == 0) echo "</tr>";
					$x ++;
				}
			?>
		</table>
		<div class="clear"></div>
	<?php } ?>
</div><!-- host -->
<div class="general">
		<h1 id="back-story">Back Story</h1>
		<p>(To be read aloud to every player at the beginning of the game)</p>
		<?php echo $backStory ?>
</div><!-- general -->
<div class="host">
		<h1 id="clues">Clues</h1>
		<p>Fold each of the following pages in half and, on its backside, write the word "Clue" with its corresponding number. Stand up the folded papers so that their contents are hidden.</p>
		<?php
		    // get number of clue images in directory
			$i = iterator_count(new DirectoryIterator('wp-content/themes/twentythirteen/images/games/' . $title)) - 4;
			for ($x = 1; $x < $i; $x ++) {
		?>
			<h2>Clue #<?php echo $x ?></h2>
			<img src="<?php echo get_template_directory_uri() ?>/images/games/<?php echo $title . "/clue-" . $x ?>.jpg" width="630" height="750">
		<?php } ?>
	</div><!-- host -->
	<?php
		$names = array();
		$result = mysqli_query($con,"SELECT name FROM players ORDER BY name ASC");
		while($row = mysqli_fetch_array($result)) {
			array_push($names, $row['name']);
		}
		$result = mysqli_query($con,"SELECT * FROM players ORDER BY name ASC");
		while($row = mysqli_fetch_array($result)) {
	?>
		<div data-character="<?php echo $row['name'] ?>">
			<div class='center' align='center'>
				<h1 id='<?php echo $row['name'] ?>'><?php echo $row['name'] ?></h1>
				<?php if ($row['role'] !== null) { ?>
					<h3>(<?php echo $row['role'] ?>)</h3>
				<?php } ?>
			</div>
			<table>
				<tr>
					<td width="25%"></td>
					<td>
						<p><?php echo $row['description'] ?></p>
						<p>(The following pages are to be read only by this character's player during the appropriate rounds of the game.)</p>
					</td>
					<td width="25%"></td>
				</tr>
			</table>
			<br><br>
			<div align="center" style="center">
				<img src="<?php echo get_template_directory_uri() ?>/images/logo-bw.png" style="width:200px;" width="200">
			</div>
			<?php if ($print == true) { ?>
				<h2>Notes</h2>
				<table cellpadding="10" cellspacing="0" border="1" bordercolor="gray" width="100%">
					<?php foreach($names as $name) {
						if ($name !== $row['name']) {
					?>
						<tr>
							<th style="text-align:right; height:50px;">
								<?php echo $name; ?>:
							</th>
							<td width="75%" height="85px"></td>
						</tr>
					<?php
							}
						}
					?>
				</table>
			<?php } ?>
			<?php for ($x = 1; $x <= 3; $x ++) { ?>
				<div class="round <?php echo $x ?>">
					<h2>Round <?php echo $x ?></h2>
					<?php echo $row['round' . $x . '_story'] ?>
					<?php if ($row['round' . $x . '_action'] != null) { ?>
						<h3>During the Round</h3>
						<?php echo $row['round' . $x . '_action'] ?>
					<?php } ?>
					<button style="display:none;" class="prev_round" data-round="<?php echo $x ?>">Previous Round</button>
					<?php if ($x < 3) { ?>
						<button class="next_round" data-round="<?php echo $x ?>">Next Round</button>
					<?php } ?>
				</div><!-- round -->
			<?php } ?>
		</div>
	<?php } ?>
	<?php
		if ($epilogue !== null) { ?>
			<div class="host">
				<h1 id="epilogue">Epilogue</h1>
				<p>(The following information is to be read aloud by the host only after the game is finished.)</p>
				<?php echo $epilogue ?>
			</div>
		<?php } ?>
	</div><!-- content -->
<?php if ($print == false) { ?>
<div id="choose-character">
	<h1>Choose Your Character</h1>
	<ul>
		<li style='margin-bottom:1em;'><a href="#" id="host">Host</a></li>
		<?php
			$result = mysqli_query($con,"SELECT * FROM players ORDER BY name ASC");
			while($row = mysqli_fetch_array($result)) {
				echo "<li style='margin-bottom:1em;'><a href='#' id='" . $row['name'] . "'>" . $row['name'] . " - " . $row['role'] . "</a></li>";
			}
		?>
	</ul>
	<script>
		jQuery("#content").hide();
		jQuery(".round").each(function() {
			if (!(jQuery(this).hasClass("1"))) jQuery(this).hide();
		});
		jQuery("#host").click(function() {
			jQuery("#content").show();
			jQuery("#content > div, #choose-character").hide();
			jQuery(".host, .general").show();
		});
		jQuery("#choose-character a").click(function() {
			if (jQuery(this).attr("id") !== "host") {
				id = jQuery(this).attr("id");
				jQuery("#content").show();
				jQuery("#content > div, #choose-character").hide();
				jQuery("div[data-character='" + id + "'], .general").show();
			}
		});
		jQuery(".next_round").click(function() {
			jQuery(".general,div[align=center], table, br").hide();
			var round = jQuery(this).attr("data-round");
			jQuery(".round." + round).hide();
			round ++;
			jQuery(".round." + round).show();
			jQuery(".prev_round").show();
			window.scrollTo(0, 0);
		});
		jQuery(".prev_round").click(function() {
			//jQuery(".general").hide();
			var round = jQuery(this).attr("data-round");
			jQuery(".round." + round).hide();
			round --;
			jQuery(".round." + round).show();
			if (round == 1) {
				jQuery(".prev_round").hide();
			}
			window.scrollTo(0, 0);
		});
	</script>
</div>
<?php } ?>
</article>
<?php mysqli_close($con); ?>